<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('protected', ['middleware' => ['auth', 'admin'], function() {
    return "this page requires that you be logged in and an Admin";
}]);

Route::get('/', function () {
    return view('welcome');
});


Auth::routes();
Route::get('/LoginFailed','Auth\AuthController@LoginFailed')->name('LoginFailed');
Route::post('login', 'Auth\AuthController@authenticate')->name('login');

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/kioskStatus', 'HomeController@kioskStatus')->name('kioskStatus');
Route::get('/approvals', 'HomeController@approvals')->name('approvals');
Route::get('/reports', 'HomeController@reports')->name('reports');


Route::get('/manage/account', 'AccountController@showItems');

Route::get('/manage/category', 'CategoryController@ShowItems');
Route::get('/manage/category/add', 'CategoryController@AddItems');
Route::get('/manage/category/update', 'CategoryController@UpdateItems');
Route::post('/manage/category/save','CategoryController@SaveItems');
Route::post('/manage/category/saveupdated','CategoryController@SaveUpdatedItems');

Route::get('/manage/map', 'MapController@showItems');

Route::get('/manage/product','ProductController@showItems');
Route::get('/manage/promo','PromoController@showItems');
Route::get('/manage/service','ServiceController@showItems');

Route::get('/manage/subscription','SubscriptionController@ShowItems');
Route::get('/manage/subscription/add', 'SubscriptionController@AddItems');
Route::post('/manage/subscription/add', 'SubscriptionController@SaveItems');
Route::get('/manage/subscription/update','SubscriptionController@UpdateItems');
Route::post('manage/subscription/savedupdated','SubscriptionController@SaveUpdatedItems');

Route::get('/manage/tag','TagController@showItems');

Route::get('/manage/event', 'EventController@ShowItems');
Route::get('/manage/event/add','EventController@AddItems');
Route::get('/manage/event/update','EventController@UpdateItems');
Route::post('/manage/event/save','EventController@SaveItems');
Route::post('/manage/event/saveupdated','EventController@SaveUpdatedItems');

Route::get('/manage/multirotator','MultirotatorController@ShowItems');
Route::get('/manage/multirotator/add','MultirotatorController@AddItems');
Route::get('/manage/multirotator/update','MultirotatorController@UpdateItems');
Route::post('/manage/multirotator/save','MultirotatorController@SaveItems');
Route::post('/manage/multirotator/saveupdated','MultirotatorController@SaveUpdatedItems');

Route::get('/manage/subscriber', 'SubscriberController@ShowItems');
Route::get('/manage/subscriber/add','SubscriberController@AddItems');
Route::post('/manage/subscriber/add/shop','SubscriberController@AddShop');
Route::get('/manage/subscriber/remove/shop/{id}','SubscriberController@RemoveShop');
Route::post('/manage/subscriber/remove/shop','SubscriberController@SaveRemovedShop');
Route::get('/manage/subscriber/update','SubscriberController@UpdateItems');
Route::post('/manage/subscriber/save','SubscriberController@SaveItems');
Route::post('/manage/subscriber/saveupdated','SubscriberController@SaveUpdatedItems');

Route::get('/manage/user', 'UserController@ShowItems');
Route::get('/manage/user/update', 'UserController@UpdateItems');
Route::post('/manage/user/approve','UserController@ApproveItems');
Route::get('/manage/user/add','UserController@AddItems');
Route::post('/manage/user/save', 'UserController@SaveItems');
Route::post('/manage/user/saveupdated','UserController@SaveUpdatedItems');
Route::post('manage/user/add/subscriber', 'UserController@AddSubscriber');
Route::get('/manage/user/remove/subscriber/{id}','UserController@RemoveSubscriber');
Route::post('/manage/user/remove/subscriber','UserController@SaveRemovedSubscriber');
Route::post('/manage/user/add/mall','UserController@AddMall');
Route::get('/manage/user/remove/mall/{id}','UserController@RemoveMall');
Route::post('/manage/user/remove/mall','UserController@SaveRemovedMall');

Route::get('/manage/shop','ShopController@ShowItems');
Route::get('/manage/shop/update','ShopController@UpdateItems');
Route::get('/manage/shop/add','ShopController@AddItems');
Route::post('/manage/shop/saveupdated','ShopController@SaveUpdatedItems');
Route::post('/manage/shop/save', 'ShopController@SaveItems');

Route::get('/manage/mallshop','MallShopController@ShowItems');
Route::get('/manage/mallshop/add','MallShopController@AddItems');
Route::get('/manage/mallshop/update','MallShopController@UpdateItems');
Route::post('/manage/mallshop/savedupdated','MallShopController@SaveUpdatedItems');
Route::post('/manage/mallshop/save', 'MallShopController@SaveItems');

Route::get('/manage/kiosk', 'KioskController@ShowItems');
Route::get('/manage/kiosk/add', 'KioskController@AddItems');
Route::get('manage/kiosk/update', 'KioskController@UpdateItems');
Route::post('manage/kiosk/savedupdated','KioskController@SaveUpdatedItems');
Route::post('manage/kiosk/save', 'KioskController@SaveItems');

Route::get('manage/product','ProductController@ShowItems');
Route::get('manage/product/add','ProductController@AddItems');
Route::post('manage/product/save', 'ProductController@SaveItems');
Route::get('manage/product/update', 'ProductController@UpdateItems');
Route::post('manage/product/savedupdated','ProductController@SaveUpdatedItems');
Route::post('manage/product/add/mall','ProductController@AddProductToMall');
Route::get('manage/product/remove/mall/{id}','ProductController@RemoveMall');
Route::post('manage/product/remove/mall','ProductController@SaveRemovedMall');
<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMultirotatorschedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('multirotatorschedules', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('mall_id');
            $table->integer('multirotator_id');
            $table->integer('shop_id'); //Null if <> belongsTo Shop
            $table->string('click_event');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('multirotatorschedules');
    }
}

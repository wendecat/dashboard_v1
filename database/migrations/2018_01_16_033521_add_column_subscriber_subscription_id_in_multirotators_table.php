<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnSubscriberSubscriptionIdInMultirotatorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('multirotators', function (Blueprint $table) {
            $table->integer('subscriber_subscription_id')->references('id')->on('subscribers_subscriptions')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('multirotators', function (Blueprint $table) {
            $table->dropColumn('subscriber_subscription_id');
        });
    }
}

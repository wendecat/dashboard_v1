<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnToProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->integer('shop_id')->default(0);
            $table->integer('brand_id')->default(0);
            $table->integer('is_active')->default(1);
            $table->integer('is_promo')->default(1);
            $table->integer('deleted_by')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('shop_id');
            $table->dropColumn('brand_id');
            $table->dropColumn('is_active');
            $table->dropColumn('is_promo');
            $table->dropColumn('deleted_by');
        });
    }
}

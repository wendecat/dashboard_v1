<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnSubscriberIdToMultirotatorschedules extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('multirotatorschedules', function (Blueprint $table) {
            $table->integer('subscriber_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('multirotatorschedules', function (Blueprint $table) {
             $table->dropColumn('subscriber_id');
        });
    }
}

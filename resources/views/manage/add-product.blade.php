@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                     <div class="navbar-container">
                        {!! Menu::get('MyNavBar')->asUl(
                            ['class' => 'nav navbar-nav nav-pills'],
                            ['class'=>'dropdown-menu']
                        ) !!}
                    </div>
                </div>

                <div class="panel-body">
                    <h2>Add Product</h2>
                    @if($errors->any())
                        <br>
                        <div class="alert alert-danger">
                          <strong>Warning!</strong> {{$errors->first()}}
                        </div>
                        <br>
                    @endif

                    {{Form::open(array('action' => 'ProductController@SaveItems', 'method' => 'post','files' => true))}}
                    {{ Form::hidden('_token', csrf_token() ) }}

                     <table class="table table-striped table-bordered table-hover table-sm">
                        <tbody>
                            <tr>
                                <td>Title:</td>
                                <td>{{ Form::text('title', null ,array('class' => 'form-control')) }}</td>
                            </tr>
                            <tr>
                                <td>Shop:</td>
                                 <td>{{ Form::select('shop', $shops, null, array('class' => 'form-control')) }}</td>
                            </tr>
                             <tr>
                                <td>Description:</td>
                                <td>{{ Form::textarea('description', null ,array('class' => 'form-control')) }}</td>
                            </tr>
                            <tr>
                                <td>Image Thumbnail:</td>
                                <td> {{ Form::file('image_thumb') }}</td>
                            </tr>
                             <tr>
                                <td>Image Enlarged:</td>
                                <td> {{ Form::file('image_large') }}</td>
                            </tr>
                            <tr>
                                <td>Show From:</td>
                                <td><div class="form-group">
                                    <div class='input-group date'>
                                        <input type='text' class="form-control"  name="show_from" id='datetimepicker_from' />
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div></td>
                            </tr>
                            <tr>
                                <td>Show To:</td>
                                <td><div class="form-group">
                                    <div class='input-group date'>
                                        <input type='text' class="form-control" name="show_to" id='datetimepicker_to' />
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div></td>
                            </tr>
                            <tr>
                                <td>Active (?):</td>
                                <td>{{ Form::select('is_active', array('' => '-----', 1 => 'Yes', 0 => 'No'), null,['class' => 'form-control']) }}</td>
                            </tr>
                             <tr>
                                <td>Promo (?):</td>
                                <td>{{ Form::select('is_promo', array('' => '-----', 1 => 'Yes', 0 => 'No'), null,['class' => 'form-control']) }}</td>
                            </tr>
                        </tbody>
                    </table>
                    
                    <button type="submit" class="btn btn-primary">Submit</button>
                    
                     {!! Form::close() !!}
                     <br>
                     <a href="/manage/product"> <button type="submit" class="btn btn-primary">View Products</button></a>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function(){
           $('#datetimepicker_from').datetimepicker({ format: 'LLL'});
           $('#datetimepicker_to').datetimepicker({ format: 'LLL'});
        });
    </script>
@endsection


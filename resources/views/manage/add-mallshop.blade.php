@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                     <div class="navbar-container">
                        {!! Menu::get('MyNavBar')->asUl(
                            ['class' => 'nav navbar-nav nav-pills'],
                            ['class'=>'dropdown-menu']
                        ) !!}
                    </div>
                </div>

                <div class="panel-body">
                    <h2>Add Mall-Shop</h2>

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    {{Form::open(array('action' => 'MallShopController@SaveItems', 'method' => 'post','files' => true))}}
                    {{ Form::hidden('_token', csrf_token() ) }}

                     <table class="table table-striped table-bordered table-hover table-sm">
                        <tbody>
                            <tr>
                                <td>Shop:</td>
                                <td>{{ Form::select('shop', $shops , null ,array('class' => 'form-control')) }}</td>
                            </tr>
                            <tr>
                                <td>Mall:</td>
                                 <td>{{ Form::select('malls', $malls, null, array('class' => 'form-control')) }}</td>
                            </tr>
                            <tr>
                                <td>Contact:</td>
                                <td>{{ Form::text('contact', null ,array('class' => 'form-control')) }}</td>
                            </tr>
                            <tr>
                                <td>Rank:</td>
                                <td>{{ Form::number('rank', null ,array('class' => 'form-control')) }}</td>
                            </tr>
                            <tr>
                                <td>Description:</td>
                                 <td>{{ Form::textarea('description', null ,array('class' => 'form-control')) }}</td>
                            </tr>
                            <tr>
                                <td>Email:</td>
                                <td>{{ Form::text('email', null ,array('class' => 'form-control')) }}</td>
                            </tr>
                            <tr>
                                <td>Space Number:</td>
                                 <td>{{ Form::text('space_number', null ,array('class' => 'form-control')) }}</td>
                            </tr>
                             <tr>
                                <td>Location Description:</td>
                                 <td>{{ Form::textarea('location_description', null ,array('class' => 'form-control')) }}</td>
                            </tr>
                             <tr>
                                <td>Active (?):</td>
                                <td>{{ Form::select('is_active', array('' => '-----', 1 => 'Yes', 0 => 'No'), null,['class' => 'form-control']) }}</td>
                            </tr>
                        </tbody>
                    </table>
                    
                    <button type="submit" class="btn btn-primary">Submit</button>
                    
                     {!! Form::close() !!}
                     <br>
                     <a href="/manage/mallshop"> <button type="submit" class="btn btn-primary">View Mall-Shops</button></a>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
    <script type="text/javascript">

    </script>
@endsection


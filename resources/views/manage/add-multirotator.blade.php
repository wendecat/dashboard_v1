@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
					 <div class="navbar-container">
                        {!! Menu::get('MyNavBar')->asUl(
                            ['class' => 'nav navbar-nav nav-pills'],
                            ['class'=>'dropdown-menu']
                        ) !!}
                    </div>
				</div>

                <div class="panel-body">
                    <h2>Add Banner Advertisement</h2>

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    {{Form::open(array('action' => 'MultirotatorController@SaveItems', 'method' => 'post','files' => true))}}
                         {{ Form::hidden('_token', csrf_token() ) }}
                     <table class="table table-striped table-bordered table-hover table-sm">
                        <tbody>
                             <tr>
                                <td>Subscription:</td>
                                <td>{{ Form::select('subscriptions', $subscriptions, '',['class' => 'form-control']) }}</td>
                            </tr>
                            <tr>
                                <td>Name:</td>
                                <td>{{ Form::text('name', null ,array('class' => 'form-control')) }}</td>
                            </tr>
                            <tr>
                                <td>Mall:</td>
                                <td>
                                    @foreach ($malls as $mall)                    
                                        <div class="form-check">
                                          <label class="form-check-label">
                                            <input class="form-check-input" type="checkbox" name="malls[]" value="{{$mall->id}}">
                                                {{$mall->name}}
                                          </label>
                                        </div>   
                                    @endforeach 
                                </td>
                            </tr>
                            <tr>
                                <td>Subscriber:</td>
                                <td>{{ Form::select('subscribers', $subscribers, '',['class' => 'form-control']) }}</td>
                            </tr>
                            <tr>
                                <td>Shop:</td>
                                <td>{{ Form::select('shops', $shops, '',['class' => 'form-control']) }}</td>
                            </tr>
                             <tr>
                                <td>Duration (sec) :</td>
                                <td>{{ Form::number('duration', null, array('class' => 'form-control')) }}</td>
                            </tr>
                            <tr>
                                <td>Start date:</td>
                                <td><div class="form-group">
                                    <div class='input-group date'>
                                        <input type='text' class="form-control"  name="start_date" id='datetimepicker_from' />
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div></td>
                            </tr>
                            <tr>
                                <td>End date:</td>
                                <td><div class="form-group">
                                    <div class='input-group date'>
                                        <input type='text' class="form-control" name="end_date" id='datetimepicker_to' />
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div></td>
                            </tr>
                            <tr>
                                <td>Image:</td>
                                <td> {{ Form::file('image') }}</td>
                            </tr>
                        </tbody>
                    </table>
                   <button type="submit" class="btn btn-primary">Submit</button>
                    
                     {!! Form::close() !!}
                     <br>
                     <a href="/manage/multirotator"> <button type="submit" class="btn btn-primary">View Multirotator</button></a>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function(){
           $('#datetimepicker_from').datetimepicker({ format: 'LLL'});
           $('#datetimepicker_to').datetimepicker({ format: 'LLL'});
        });
</script>
@endsection

<!-- 
    TODO: add filter by mall
    if account is admin, show all malls
    else show only malls allowed for user
-->

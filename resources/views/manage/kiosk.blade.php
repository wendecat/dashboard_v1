@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                     <div class="navbar-container">
                        {!! Menu::get('MyNavBar')->asUl(
                            ['class' => 'nav navbar-nav nav-pills'],
                            ['class'=>'dropdown-menu']
                        ) !!}
                    </div>
                </div>

                <div class="panel-body">
                    <h2>Kioks</h2>
                
                    {{Form::open(array('action' => 'KioskController@UpdateItems', 'method' => 'get'))}}
                        {{ Form::hidden('_token', csrf_token() ) }}
                        <table class="table table-striped table-bordered table-hover table-sm">
                           <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Mall</th>
                                    <th>Name</th>
                                    <th>Last Seen</th>
                                    <th>Active (?)</th>
                                    <th>
                                        <div class="btn-group dropdown">
                                          <button type="submit" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Modify</button>
                                          <ul class="dropdown-menu">
                                            <li>{!! Form::submit( 'Delete', ['class' => 'btn btn-default', 'name' => 'submit', 'value' => 'delete'])!!}</li>
                                            <li>{!! Form::submit( 'Update', ['class' => 'btn btn-default', 'name' => 'submit', 'value' => 'update']) !!}</li>
                                          </ul>
                                        </div>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($items as $item)
                                        <tr>
                                            <th scope="row">{{$loop->iteration}}</th>
                                            <td>{{$item->Mall['name']}}</td>
                                            <td>{{$item->name}}</td>
                                            <td>@if($item->last_seen == '1970-01-01 08:00:00') Never @else {{$item->last_seen}}@endif</td>
                                            <td>@if($item->is_active == 1) Yes @else No @endif</td>
                                            <td>{{ Form::checkbox('selected[]',$item->id) }}</td>
                                        </tr>
                                @endforeach
                            </tbody>
                             <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Mall</th>
                                    <th>Name</th>
                                    <th>Last Seen</th>
                                    <th>Active (?)</th>
                                    <th>
                                        <div class="btn-group dropdown">
                                          <button type="submit" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Modify</button>
                                          <ul class="dropdown-menu">
                                            <li>{!! Form::submit( 'Delete', ['class' => 'btn btn-default', 'name' => 'submit', 'value' => 'delete'])!!}</li>
                                            <li>{!! Form::submit( 'Update', ['class' => 'btn btn-default', 'name' => 'submit', 'value' => 'update']) !!}</li>
                                          </ul>
                                        </div>
                                    </th>
                                </tr>
                            </thead>
                        </table>
                    {!! Form::close() !!}
                    <br>              
                    <a href="/manage/kiosk/add"><button type="submit" class="btn btn-primary">Add</button></a>
                   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function(){
            $('.table').DataTable();
        });
    </script>
@endsection
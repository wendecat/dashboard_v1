@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
					 <div class="navbar-container">
                        {!! Menu::get('MyNavBar')->asUl(
                            ['class' => 'nav navbar-nav nav-pills'],
                            ['class'=>'dropdown-menu']
                        ) !!}
                    </div>
				</div>

                <div class="panel-body">
                    <h2>Banner Advertisement</h2>

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    {{Form::open(array('action' => 'MultirotatorController@UpdateItems', 'method' => 'get'))}}
                    {{ Form::hidden('_token', csrf_token() ) }}
                    @if(count($items) > 0)
                     <table class="table table-striped table-bordered table-hover table-sm">
                       <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Image</th>
                                <th>Duration (sec.)</th>
                                <th>Mall</th>
                                <th>Subscriber</th>
                                <th>Shop</th>
                                <th>Show From</th>
                                <th>Show To</th>
                                <th>
                                    <div class="btn-group dropdown">
                                      <button type="submit" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Modify</button>
                                      <ul class="dropdown-menu">
                                        <li>{!! Form::submit( 'Delete', ['class' => 'btn btn-default', 'name' => 'submit', 'value' => 'delete'])!!}</li>
                                        <li>{!! Form::submit( 'Update', ['class' => 'btn btn-default', 'name' => 'submit', 'value' => 'update']) !!}</li>
                                      </ul>
                                    </div>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($items as $item)
                                <tr>
                                    <th scope="row">{{$loop->iteration}}</th>
                                    <td>{{$item->multirotator->name}}</td>
                                    <td><img src="{{ asset('/storage/images/multirotator/' . $item->multirotator->image) }}" height="100px"/></td>
                                    <td>{{$item->multirotator->duration}}</td>
                                    <td>{{$item->mall->name}}</td>
                                    @if($item->subscriber === null)
                                        <td>Non-Subscriber</td> 
                                    @else 
                                        <td>{{$item->subscriber->name}}</td>
                                    @endif
                                    <td>Shop goes here</td>
                                    <td>{{$item->start_date}}</td>
                                    <td>{{$item->end_date}}</td>
                                    <td>{{ Form::checkbox('selected[]',$item->id) }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Image</th>
                                <th>Duration (sec.)</th>
                                <th>Mall</th>
                                <th>Subscriber</th>
                                <th>Shop</th>
                                <th>Show From</th>
                                <th>Show To</th>
                                <th>
                                    <div class="btn-group dropdown">
                                      <button type="submit" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Modify</button>
                                      <ul class="dropdown-menu">
                                        <li>{!! Form::submit( 'Delete', ['class' => 'btn btn-default', 'name' => 'submit', 'value' => 'delete'])!!}</li>
                                        <li>{!! Form::submit( 'Update', ['class' => 'btn btn-default', 'name' => 'submit', 'value' => 'update']) !!}</li>
                                      </ul>
                                    </div>
                                </th>
                            </tr>
                        </thead>
                    </table>

                     @else
                        <div class="card">
                          <div class="card-block text-nowrap">
                            Oops! It seems you don't have any subscriptions yet.
                          </div>
                        </div>
                    @endif
                    {!! Form::close() !!}
                    <br>
                    <!--TODO:  add must first check if has subscription. If none, don't show-->                       
                    <a href="/manage/multirotator/add"><button type="submit" class="btn btn-primary">Add</button></a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

<!-- 
    TODO: create relationship for multirotator and subscribers
    TODO: add filter Mall
    TODO: add filter Subscriber

-->

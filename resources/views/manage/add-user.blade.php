@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
					 <div class="navbar-container">
                        {!! Menu::get('MyNavBar')->asUl(
                            ['class' => 'nav navbar-nav nav-pills'],
                            ['class'=>'dropdown-menu']
                        ) !!}
                    </div>
				</div>
               
                <div class="panel-body">
                    <h2>Add User</h2>
                    
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    {{Form::open(array('action' => 'UserController@SaveItems', 'method' => 'post','files' => true))}}
                         {{ Form::hidden('_token', csrf_token() ) }}
                     <table class="table table-striped table-bordered table-hover table-sm">
                        <tbody>
                            <tr>
                                <td>Name:</td>
                                <td>{{ Form::text('name', null ,array('class' => 'form-control')) }}</td>
                            </tr>
                            <tr>
                                <td>Email:</td>
                                <td>{{ Form::text('email', null, array('class' => 'form-control')) }}</td>
                            </tr>
                            <tr>
                                <td>Password:</td>
                                <td>{{ Form::password('password', array('class' => 'form-control')) }}</td>
                            </tr>
                            <tr>
                                <td>Role:</td>
                                <td>{{Form::select('role', $items , null ,['class' => 'form-control'])}}</td>
                            </tr>
                            <tr>
                                <td>Active (?):</td>
                                <td>{{Form::select('is_active', array('' => '-----', 1 => 'Yes', 0 => 'No'),  '',['class' => 'form-control'])}}</td>
                            </tr>
                            <tr>
                                <td>Aprroved (?):</td>
                                <td>{{Form::select('is_approved', array('' => '-----', 1 => 'Yes', 0 => 'No'), '', ['class' => 'form-control'])}}</td>
                            </tr>


                        </tbody>
                    </table>
                    <button type="submit" class="btn btn-primary">Submit</button>
                    
                     {!! Form::close() !!}
                     <br>
                     <a href="/manage/user"> <button type="submit" class="btn btn-primary">View Users</button></a>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function(){

        });
</script>
@endsection


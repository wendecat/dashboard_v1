@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
					 <div class="navbar-container">
                        {!! Menu::get('MyNavBar')->asUl(
                            ['class' => 'nav navbar-nav nav-pills'],
                            ['class'=>'dropdown-menu']
                        ) !!}
                    </div>
				</div>

                <div class="panel-body">
                    <h2>Unassociate Malls</h2>
                    <br>

                    {{Form::open(array('action' => 'UserController@SaveRemovedMall', 'method' => 'post'))}}
                    {{ Form::hidden('_token', csrf_token() ) }}
                    {{ Form::hidden('user_id', $user[0]->id ) }}

                    @if(count($items) > 0)
                     <table class="table table-striped table-bordered table-hover table-sm">
                       <thead>
                            <tr>
                                <th>#</th>
                                <th>Title</th> 
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($items as $item)
                                <!-- <tr class="table-tr" data-url="/manage/subscriber/add/shop/{{$item->id}}"> -->
                                <tr>
                                    <th scope="row">{{$loop->iteration}}</th>
                                    <td>{{$item->mall->name}}</td>
                                    <td><input type="checkbox" name="selected[]" value="{{$item->mall->id}}" /></td>
                                </tr>
                            @endforeach
                        </tbody>
                        <thead>
                           <tr>
                                <th>#</th>
                                <th>Title</th>
                                <th></th>
                            </tr>
                        </thead>
                    </table>
                    @endif
                    <br>
                    <button type="submit" class="btn btn-primary">Submit</button>
                    {!! Form::close() !!}
                     <br>
                     <a href="/manage/user"> <button type="submit" class="btn btn-primary">View Users</button></a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script type="text/javascript">

    </script>
@endsection
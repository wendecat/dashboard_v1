@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
					 <div class="navbar-container">
                        {!! Menu::get('MyNavBar')->asUl(
                            ['class' => 'nav navbar-nav nav-pills'],
                            ['class'=>'dropdown-menu']
                        ) !!}
                    </div>
				</div>

                <div class="panel-body">
                    <h2>Update Kiosk</h2>

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    
                    {{Form::open(array('action' => 'KioskController@SaveUpdatedItems', 'method' => 'post','files' => true))}}
                    {{ Form::hidden('_token', csrf_token() ) }}

                     @foreach($items as $item)
                     {{ Form::hidden('id[]', $item->id) }}
                     <table class="table table-striped table-bordered table-hover table-sm">
                        <tbody>
                            <tr>
                                <td colspan="2"><h4>Item # {{$loop->iteration}}</h4></td>
                            </tr>
                            <tr>
                                <td>Mall:</td>
                                <td>{{ Form::select('mall[]', $malls, $item->Mall['id'], array('class' => 'form-control')) }}</td>
                            </tr>
                            <tr>
                                <td>Name:</td>
                                <td>{{ Form::textarea('name[]', $item->name, array('class' => 'form-control')) }}</td>
                            </tr>
                            <tr>
                                <td>Active (?):</td>
                                <td>{{ Form::select('is_active[]', array('' => '-----', 1 => 'Yes', 0 => 'No'),  $item->is_active,['class' => 'form-control']) }}</td>
                            </tr>
                                                      
                        </tbody>
                    </table>
                    @endforeach
                    <button type="submit" class="btn btn-primary">Submit</button>
                     {!! Form::close() !!}
                     <br>
                     <a href="/manage/kiosk"> <button type="submit" class="btn btn-primary">View Kiosks</button></a>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function(){
          
        });
</script>
@endsection

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                     <div class="navbar-container">
                        {!! Menu::get('MyNavBar')->asUl(
                            ['class' => 'nav navbar-nav nav-pills'],
                            ['class'=>'dropdown-menu']
                        ) !!}
                    </div>
                </div>

                <div class="panel-body">
                    <h2>Admin Subscriptions Page</h2>
                    <!--
                        TODO: break down display based on subscriber, subscription,
                    -->

                     
                    <!--
                        TODO: break down display based on subscriber, subscription,
                    -->

                     <table class="table table-striped table-bordered table-hover table-sm">
                       <thead>
                            <tr>
                                <th>#</th>
                                <th>Subscriber</th>
                                <th>Subscription</th>
                                <th>Start Date</th>
                                <th>End Date</th>
                            </tr>
                            <tr>
                                <td>Filter by</td>
                                <td>{{Form::select('subscribers', $subscribers, '',['class' => 'subscribers'])}}</td>
                                <td>{{Form::select('subscriptions', $subscriptions, '',['class' => 'subscriptions'])}}</td>
                                <td></td>
                                <td></td>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach ($items as $item)

                                <tr class="{{$item->subscriber->subscriber_code}} {{$item->subscription->subscription_code}}">
                                    <th scope="row">{{$loop->iteration}}</th>
                                    <td>{{$item->subscriber->name}} </td>
                                    <td>{{$item->subscription->name}}</td>
                                    <td>{{$item->start_date}}</td>
                                    <td>{{$item->end_date}}</td>
                                </tr>

                            @endforeach

                        </tbody>
                    </table>


                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script type="text/javascript">

        $( document ).ready(function() {
            $('.subscribers, .subscriptions').on('change' , function() {
                //alert(  $('.subscribers').val());
                if(this.value == ''){
                    $('.table tbody').children().show();
                }else{
                    $('.table tbody').children().show();
                    $('.table tbody').children().not('.' + this.value).hide();
                }

            });
        });

    </script>
@endsection

<!--
    TODO: group showing of items based on subscriber, subscription
    TODO: compute total based on subscriber, subscription
    TODO: show items based on multiple select values
-->
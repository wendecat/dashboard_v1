@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                     <div class="navbar-container">
                        {!! Menu::get('MyNavBar')->asUl(
                            ['class' => 'nav navbar-nav nav-pills'],
                            ['class'=>'dropdown-menu']
                        ) !!}
                    </div>
                </div>

                <div class="panel-body">
                    <h2>Mall Associated Shops</h2>

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                
                    {{Form::open(array('action' => 'MallShopController@UpdateItems', 'method' => 'get'))}}
                        {{ Form::hidden('_token', csrf_token() ) }}
                        <table class="table table-striped table-bordered table-hover table-sm">
                           <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Title</th>
                                    <th>Mall</th>
                                    <th>Contact</th>
                                    <th>Rank</th>
                                    <th>Description</th>
                                    <th>Email</th>
                                    <th>Space Number</th>
                                    <th>Active (?)</th>
                                    <th>
                                        <div class="btn-group dropdown">
                                          <button type="submit" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Modify</button>
                                          <ul class="dropdown-menu">
                                            <li>{!! Form::submit( 'Delete', ['class' => 'btn btn-default', 'name' => 'submit', 'value' => 'delete'])!!}</li>
                                            <li>{!! Form::submit( 'Update', ['class' => 'btn btn-default', 'name' => 'submit', 'value' => 'update']) !!}</li>
                                          </ul>
                                        </div>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($items as $item)
                                        <tr>
                                            <th scope="row">{{$loop->iteration}}</th>
                                            <td>{{$item->shop['title']}}</td>
                                            <td>{{$item->mall['name']}}</td>
                                            <td>{{ $item->contact }} </td>
                                            <td>{{ $item->rank }}</td>
                                            <td>{{ $item->description }}</td>
                                            <td>{{ $item->email }} </td>
                                            <td>{{ $item->space_number }}</td>
                                            <td>@if($item->is_active === 1) Yes @else No @endif</td>
                                            <td>{{ Form::checkbox('selected[]',$item->id) }}</td>
                                        </tr>
                                @endforeach
                            </tbody>
                             <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Title</th>
                                    <th>Mall</th>
                                    <th>Contact</th>
                                    <th>Rank</th>
                                    <th>Description</th>
                                    <th>Email</th>
                                    <th>Space Number</th>
                                    <th>Active (?)</th>
                                    <th>
                                        <div class="btn-group dropdown">
                                          <button type="submit" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Modify</button>
                                          <ul class="dropdown-menu">
                                            <li>{!! Form::submit( 'Delete', ['class' => 'btn btn-default', 'name' => 'submit', 'value' => 'delete'])!!}</li>
                                            <li>{!! Form::submit( 'Update', ['class' => 'btn btn-default', 'name' => 'submit', 'value' => 'update']) !!}</li>
                                          </ul>
                                        </div>
                                    </th>
                                </tr>
                            </thead>
                        </table>
                    {!! Form::close() !!}
                    <br>              
                    <a href="/manage/mallshop/add"><button type="submit" class="btn btn-primary">Add</button></a>
                   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function(){
            $('.table').DataTable();
        });
    </script>
@endsection
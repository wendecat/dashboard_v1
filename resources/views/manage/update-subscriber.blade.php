@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
					 <div class="navbar-container">
                        {!! Menu::get('MyNavBar')->asUl(
                            ['class' => 'nav navbar-nav nav-pills'],
                            ['class'=>'dropdown-menu']
                        ) !!}
                    </div>
				</div>

                <div class="panel-body">
                    <h2>Update Subscriber</h2>

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    
                    {{Form::open(array('action' => 'SubscriberController@SaveUpdatedItems', 'method' => 'post','files' => true))}}
                    {{ Form::hidden('_token', csrf_token() ) }}

                     @foreach($items as $item)
                     {{ Form::hidden('id', $item->id) }}
                     <table class="table table-striped table-bordered table-hover table-sm">
                        <tbody>
                            <tr>
                                <td>Name:</td>
                                <td>{{ Form::text('name', $item->name ,array('class' => 'form-control')) }}</td>
                            </tr>
                            <tr>
                                <td>Address:</td>
                                <td>{{ Form::textarea('address', $item->address, array('class' => 'form-control')) }}</td>
                            </tr>
                            <tr>
                                <td>Contact:</td>
                                <td>{{Form::text('contact', $item->contact, ['class' => 'form-control'])}}</td>
                            </tr>
                            <tr>
                                <td>Email:</td>
                                <td>{{Form::text('email', $item->email, ['class' => 'form-control'])}}</td>
                            </tr>     
                        </tbody>
                    </table>
                     @endforeach
                    <button type="submit" class="btn btn-primary">Submit</button>
                     {!! Form::close() !!}
                     <br>
                     <a href="/manage/subscriber"> <button type="submit" class="btn btn-primary">View Subscribers</button></a>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
    <script type="text/javascript">

    </script>
@endsection

<!-- 
    TODO: add filter by mall
    if account is admin, show all malls
    else show only malls allowed for user

    Tue, Oct 17, 2017 11:39 PM
    D, M d, Y h:i A

    dynamically create datetimepicker instance and make it work in js

-->

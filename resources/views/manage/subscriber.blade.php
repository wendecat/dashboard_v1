@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
					 <div class="navbar-container">
                        {!! Menu::get('MyNavBar')->asUl(
                            ['class' => 'nav navbar-nav nav-pills'],
                            ['class'=>'dropdown-menu']
                        ) !!}
                    </div>
				</div>

                <div class="panel-body">
                    <h2>Subscribers</h2>

                    @if($errors->any())
                        <br>
                        <div class="alert alert-danger">
                          <strong>Warning!</strong> {{$errors->first()}}
                        </div>
                        <br>
                    @endif

                    {{Form::open(array('action' => 'SubscriberController@UpdateItems', 'method' => 'get'))}}
                    {{ Form::hidden('_token', csrf_token() ) }}
                    @if(count($items) > 0)
                     <table class="table table-striped table-bordered table-hover table-sm">
                       <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Address</th>
                                <th>Contact</th>
                                <th>Email</th>
                                <th>Active (?)</th>
                                <th>
                                    <div class="btn-group dropdown">
                                      <button type="submit" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Modify</button>
                                      <ul class="dropdown-menu">
                                        <li>{!! Form::submit( 'Delete', ['class' => 'btn btn-default', 'name' => 'submit', 'value' => 'delete'])!!}</li>
                                        <li>{!! Form::submit( 'Update', ['class' => 'btn btn-default', 'name' => 'submit', 'value' => 'update']) !!}</li>
                                        <li>{!! Form::submit( 'Associate Shops', ['class' => 'btn btn-default', 'name' => 'submit']) !!}</li>
                                        <li>{!! Form::submit( 'Remove Shops Association', ['class' => 'btn btn-default', 'name' => 'submit']) !!}</li>
                                       <!--  <li>{!! Form::submit( 'Add Subscription', ['class' => 'btn btn-default', 'name' => 'submit']) !!}</li>
                                        <li>{!! Form::submit( 'View Subscription', ['class' => 'btn btn-default', 'name' => 'submit']) !!}</li> -->
                                      </ul>
                                    </div>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($items as $item)
                                <tr class="table-tr" data-url="/manage/subscriber/add/shop/{{$item->id}}">
                                    <th scope="row">{{$loop->iteration}}</th>
                                    <td>{{$item->name}}</td>
                                    <td>{{$item->address}}</td>
                                    <td>{{$item->contact}}</td>
                                    <td>{{$item->email}}</td>
                                    <td>@if($item->is_active ==1) Yes @else No @endif</td>
                                    <td>{{ Form::checkbox('selected[]',$item->id) }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Address</th>
                                <th>Contact</th>
                                <th>Email</th>
                                <th>
                                    <div class="btn-group dropdown">
                                      <button type="submit" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Modify</button>
                                      <ul class="dropdown-menu">
                                        <li>{!! Form::submit( 'Delete', ['class' => 'btn btn-default', 'name' => 'submit', 'value' => 'delete'])!!}</li>
                                        <li>{!! Form::submit( 'Update', ['class' => 'btn btn-default', 'name' => 'submit', 'value' => 'update']) !!}</li>
                                        <li>{!! Form::submit( 'Associate Shops', ['class' => 'btn btn-default', 'name' => 'submit']) !!}</li>
                                        <li>{!! Form::submit( 'Remove Shops Association', ['class' => 'btn btn-default', 'name' => 'submit']) !!}</li>
                                       <!--  <li>{!! Form::submit( 'Add Subscription', ['class' => 'btn btn-default', 'name' => 'submit']) !!}</li>
                                        <li>{!! Form::submit( 'View Subscription', ['class' => 'btn btn-default', 'name' => 'submit']) !!}</li> -->
                                      </ul>
                                    </div>
                                </th>
                            </tr>
                        </thead>
                    </table>

                     @else
                        <div class="card">
                          <div class="card-block text-nowrap">
                            Oops! It seems there are no subscribers. Go make a SALE! :-)
                          </div>
                        </div>
                    @endif
                    {!! Form::close() !!}
                    <br>
                    <!--TODO:  add must first check if has subscription. If none, don't show-->                       
                    <a href="/manage/subscriber/add"><button type="submit" class="btn btn-primary">Add</button></a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

<!-- 
    TODO: create relationship for multirotator and subscribers

-->

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function(){
            $('.table').DataTable();
        });
    </script>
@endsection
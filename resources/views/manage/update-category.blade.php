@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                     <div class="navbar-container">
                        {!! Menu::get('MyNavBar')->asUl(
                            ['class' => 'nav navbar-nav nav-pills'],
                            ['class'=>'dropdown-menu']
                        ) !!}
                    </div>
                </div>

                <div class="panel-body">
                    <h2>Update Category</h2>

                     @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    
                    {{Form::open(array('action' => 'CategoryController@SaveUpdatedItems', 'method' => 'post','files' => true))}}
                         {{ Form::hidden('_token', csrf_token() ) }}

                     @foreach($items as $item)
                     {{ Form::hidden('id[]', $item->id) }}
                     <table class="table table-striped table-bordered table-hover table-sm">
                        <tbody>
                            <tr>
                                <td>Name:</td>
                                <td>{{ Form::text('name[]', $item->name ,array('class' => 'form-control')) }}</td>
                            </tr>
                            <tr>
                                <td>Group:</td>
                                <td>{{ Form::select('group[]', $group, $item->group_id, array('class' => 'form-control')) }}</td>
                            </tr>
                             <tr>
                                <td>Active (?):</td>
                                <td>{{Form::select('is_active[]', array('' => '-----', 1 => 'Yes', 0 => 'No'),  $item->is_active,['class' => 'form-control'])}}</td>
                            </tr>
                        </tbody>
                    </table>
                    @endforeach
                    <button type="submit" class="btn btn-primary">Submit</button>
                    
                     {!! Form::close() !!}
                     <br>
                     <a href="/manage/category"> <button type="submit" class="btn btn-primary">View Categories</button></a>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
    <script type="text/javascript">

    </script>
@endsection


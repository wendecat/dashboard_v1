@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
					 <div class="navbar-container">
                        {!! Menu::get('MyNavBar')->asUl(
                            ['class' => 'nav navbar-nav nav-pills'],
                            ['class'=>'dropdown-menu']
                        ) !!}
                    </div>
				</div>

                <div class="panel-body">
                    <h2>Associate Subscribers</h2>
                    <br>
                    <div class="alert alert-info">
                        <strong>Associated subscribers with {{ $user[0]->name}}: </strong> 
                        @foreach($subscribers as $subscriber)
                            {{$subscriber->subscriber->name}},
                        @endforeach
                    </div>

                    <a href="/manage/user/remove/subscriber/{{$user[0]->id}} "> <button type="submit" class="btn btn-primary">Remove Associated Subscriber</button></a>
                    
                    <br><br>
                    
                    <div class="alert alert-info info-indicator">
                        <strong>Subscriber(s) to be added: </strong>
                    </div>
                    
                    @if($errors->any())
                        <br>
                        <div class="alert alert-danger">
                          <strong>Warning!</strong> {{$errors->first()}}
                        </div>
                        <br>
                    @endif

                    {{Form::open(array('action' => 'UserController@AddSubscriber', 'method' => 'post'))}}
                    {{ Form::hidden('_token', csrf_token() ) }}
                    {{ Form::hidden('user_id', $user[0]->id ) }}
                   
                     @if(count($items) > 0)
                     <table class="table table-striped table-bordered table-hover table-sm">
                       <thead>
                            <tr>
                                <th>#</th>
                                <th>Title</th> 
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($items as $item)
                                <!-- <tr class="table-tr" data-url="/manage/subscriber/add/shop/{{$item->id}}"> -->
                                <tr>
                                    <th scope="row">{{$loop->iteration}}</th>
                                    <td>{{$item->name}}</td>
                                    <td><input type="checkbox" name="selected[]" value="{{$item->id}}" title="{{$item->name}}" /></td><!--TODO: checkbox must be checked if $item->id in $shop->id  -->
                                </tr>
                            @endforeach
                        </tbody>
                        <thead>
                           <tr>
                                <th>#</th>
                                <th>Title</th>
                                <th></th>
                            </tr>
                        </thead>
                    </table>
                    @endif
                    <br>
                    <button type="submit" class="btn btn-primary">Submit</button>
                    {!! Form::close() !!}
                     <br>
                     <a href="/manage/user"> <button type="submit" class="btn btn-primary">View Users</button></a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function(){
            $('.table').DataTable();
        });

        $( ":checkbox" ).change(function(){
            var title = $(this).attr('title');
            var id = this.value;
            if(this.checked){
                $('.info-indicator').append('<span id="' + id +'"> ' +  title +  ', </span>');
            }else{
                 $('#' + id).remove();
            }

        });
        
    </script>
@endsection
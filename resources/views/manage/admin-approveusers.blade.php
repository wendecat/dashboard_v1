@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
					 <div class="navbar-container">
                        {!! Menu::get('MyNavBar')->asUl(
                            ['class' => 'nav navbar-nav nav-pills'],
                            ['class'=>'dropdown-menu']
                        ) !!}
                    </div>
				</div>

                {{Form::open(array('action' => 'UserController@ApproveItems', 'method' => 'post'))}}
                <div class="panel-body">
                    <h2>Approve Users</h2>
                    <table class="table table-striped table-bordered table-hover table-sm">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <!-- <th>Designation</th> -->
                            <th>Email</th>
                            <th>Approved (?)</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($items as $item)
                            <tr>
                                <th scope="row">{{$loop->iteration}}</th>
                                <td>{{$item->name}}</td>
                                <!-- <td>{{$item->designation}}</td> -->
                                <td>{{$item->email}}</td>
                                <td>{{Form::select('status', ['0' => '----', '1' => 'Yes', '2' => 'No'])}}</td>
                            </tr>
                            {{ Form::hidden('id', $item->id) }}
                        @endforeach
                        </tbody>
                    </table>
                    {{Form::submit('Submit', ['class'=>'btn btn-primary'])}}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection

<script>

</script>

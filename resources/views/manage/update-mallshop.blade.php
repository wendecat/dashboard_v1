@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                     <div class="navbar-container">
                        {!! Menu::get('MyNavBar')->asUl(
                            ['class' => 'nav navbar-nav nav-pills'],
                            ['class'=>'dropdown-menu']
                        ) !!}
                    </div>
                </div>

                <div class="panel-body">
                    <h2>Update Mall-Shop</h2>

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    {{Form::open(array('action' => 'MallShopController@SaveUpdatedItems', 'method' => 'post','files' => true))}}
                    {{ Form::hidden('_token', csrf_token() ) }}

                     @foreach($items as $item)
                     {{ Form::hidden('id', $item->id) }}
                     <?php /**/ if($item->Category != null){ $category_id = $item->Category->id; }  /**/ ?>
                     <table class="table table-striped table-bordered table-hover table-sm">
                        <tbody>
                            <tr>
                                <td>Shop:</td>
                                <td>{{ Form::select('shop', $shops ,$item->shop['id'] ,array('class' => 'form-control')) }}</td>
                            </tr>
                            <tr>
                                <td>Mall:</td>
                                 <td>{{ Form::select('malls', $malls, $item->mall['id'], array('class' => 'form-control')) }}</td>
                            </tr>
                            <tr>
                                <td>Contact:</td>
                                <td>{{ Form::text('contact', $item->contact ,array('class' => 'form-control')) }}</td>
                            </tr>
                            <tr>
                                <td>Rank:</td>
                                <td>{{ Form::number('rank', $item->rank ,array('class' => 'form-control')) }}</td>
                            </tr>
                            <tr>
                                <td>Description:</td>
                                 <td>{{ Form::textarea('description', $item->description ,array('class' => 'form-control')) }}</td>
                            </tr>
                            <tr>
                                <td>Email:</td>
                                <td>{{ Form::text('email', $item->email ,array('class' => 'form-control')) }}</td>
                            </tr>
                            <tr>
                                <td>Space Number:</td>
                                 <td>{{ Form::text('space_number', $item->space_number ,array('class' => 'form-control')) }}</td>
                            </tr>
                             <tr>
                                <td>Location Description:</td>
                                 <td>{{ Form::textarea('location_description', $item->location_description ,array('class' => 'form-control')) }}</td>
                            </tr>
                             <tr>
                                <td>Active (?):</td>
                                <td>{{ Form::select('is_active', array('' => '-----', 1 => 'Yes', 0 => 'No'),  $item->is_active,['class' => 'form-control']) }}</td>
                            </tr>
                        </tbody>
                    </table>
                    @endforeach
                    <button type="submit" class="btn btn-primary">Submit</button>
                    
                     {!! Form::close() !!}
                     <br>
                     <a href="/manage/shop"> <button type="submit" class="btn btn-primary">View Mall-Shops</button></a>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
    <script type="text/javascript">

    </script>
@endsection


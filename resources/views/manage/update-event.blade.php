@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
					 <div class="navbar-container">
                        {!! Menu::get('MyNavBar')->asUl(
                            ['class' => 'nav navbar-nav nav-pills'],
                            ['class'=>'dropdown-menu']
                        ) !!}
                    </div>
				</div>

                <div class="panel-body">
                    <h2>Update Event</h2>

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    
                    {{Form::open(array('action' => 'EventController@SaveUpdatedItems', 'method' => 'post','files' => true))}}
                    {{ Form::hidden('_token', csrf_token() ) }}

                     @foreach($items as $item)
                     {{ Form::hidden('id', $item->id) }}
                     <table class="table table-striped table-bordered table-hover table-sm">
                        <tbody>
                            <!-- <tr>
                                <td colspan="2"><h4>Item # {{$loop->iteration}}</h4></td>
                            </tr> -->
                            <tr>
                                <td>Title:</td>
                                <td>{{ Form::text('title', $item->title, array('class' => 'form-control')) }}</td>
                            </tr>
                            <tr>
                                <td>Description:</td>
                                <td>{{ Form::textarea('description', $item->description, array('class' => 'form-control')) }}</td>
                            </tr>
                            <tr>
                                <td>Mall:</td>
                                <td>{{Form::select('malls', $malls, $item->mall_id,['class' => 'form-control malls'])}}</td>
                            </tr>
                            <tr>
                                <td>Show From:</td>
                                <td><div class="form-group">
                                    <div class='input-group date'>
                                        <input type='text' class="form-control"  name="show_from" id='datetimepicker_from' value="{{ $item->show_from }}" />
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div></td>
                            </tr>
                            <tr>
                                <td>Show To:</td>
                                <td><div class="form-group">
                                    <div class='input-group date'>
                                        <input type='text' class="form-control" name="show_to" id='datetimepicker_to' value="{{$item->show_to}}" />
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div></td>
                            </tr>
                           <!--  <tr>
                                <td>Image:</td>
                                <td></td>
                            </tr> -->
                           
                        </tbody>
                    </table>
                     @endforeach
                    <button type="submit" class="btn btn-primary">Submit</button>
                     {!! Form::close() !!}
                     <br>
                     <a href="/manage/event"> <button type="submit" class="btn btn-primary">View Events</button></a>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function(){
           $('#datetimepicker_from').datetimepicker();
           $('#datetimepicker_to').datetimepicker();
        });

        $('#datetimepicker_from').datetimepicker({
            useCurrent: false, //this is important as the functions sets the default date value to the current value 
            format: 'YYYY-MM-DD h:mm:ss'
        });
       $('#datetimepicker_to').datetimepicker({
            useCurrent: false, //this is important as the functions sets the default date value to the current value 
            format: 'YYYY-MM-DD h:mm:ss'
        });
</script>
@endsection

<!-- 
    TODO: add filter by mall
    if account is admin, show all malls
    else show only malls allowed for user

    Tue, Oct 17, 2017 11:39 PM
    D, M d, Y h:i A

    dynamically create datetimepicker instance and make it work in js

-->

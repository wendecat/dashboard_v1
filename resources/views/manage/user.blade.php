@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
					 <div class="navbar-container">
                        {!! Menu::get('MyNavBar')->asUl(
                            ['class' => 'nav navbar-nav nav-pills'],
                            ['class'=>'dropdown-menu']
                        ) !!}
                    </div>
				</div>

                <div class="panel-body">
                     <h2>Users</h2>

                     @if($errors->any())
                        <br>
                        <div class="alert alert-danger">
                          <strong>Warning!</strong> {{$errors->first()}}
                        </div>
                        <br>
                    @endif
                    
                    {{Form::open(array('action' => 'UserController@UpdateItems', 'method' => 'get'))}}
                        {{ Form::hidden('_token', csrf_token() ) }}
                        <table class="table table-striped table-bordered table-hover table-sm">
                           <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Role</th>
                                    <th>Active (?)</th>
                                    <th>Approved (?)</th>
                                    <th>Approved by</th>
                                    <th>
                                        <div class="btn-group dropdown">
                                          <button type="submit" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Modify</button>
                                          <ul class="dropdown-menu">
                                            <li>{!! Form::submit( 'Delete', ['class' => 'btn btn-default', 'name' => 'submit', 'value' => 'delete'])!!}</li>
                                            <li>{!! Form::submit( 'Update', ['class' => 'btn btn-default', 'name' => 'submit', 'value' => 'update']) !!}</li>
                                            <li>{!! Form::submit( 'Approve Users', ['class' => 'btn btn-default', 'name' => 'submit', 'value' => 'update']) !!}</li>
                                            <li>{!! Form::submit( 'Associate Subscribers', ['class' => 'btn btn-default', 'name' => 'submit', 'value' => 'update']) !!}</li>
                                            <li>{!! Form::submit( 'Associate Malls', ['class' => 'btn btn-default', 'name' => 'submit', 'value' => 'update']) !!}</li>
                                          </ul>
                                        </div>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($items as $item) 
                                        <tr>
                                            <th scope="row">{{$loop->iteration}}</th>
                                            <td>{{$item->name}}</td>
                                            <td>{{$item->email}}</td>
                                            <td>@if(count($item->roleuser) > 0) 
                                                    @if($item->RoleUser[0]->role_id == 1) PEMI 
                                                    @elseif($item->RoleUser[0]->role_id == 2) Subscriber @elseif($item->RoleUser[0]->role_id == 3) Mall Admin 
                                                    @endif
                                                @else 
                                                @endif
                                            </td>
                                            <td>@if($item->is_active ==1) Yes @else No @endif</td>
                                            <td>@if($item->is_approved ==1) Yes @else No @endif</td>
                                            <td>@if(count($item->Parent) > 0){{$item->Parent->name}} @endif</td>
                                            <td>{{ Form::checkbox('selected[]', $item->id) }}</td>
                                        </tr>
                                @endforeach
                            </tbody>
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Role</th>
                                    <th>Active (?)</th>
                                    <th>Approved (?)</th>
                                    <th>Approved by</th>
                                    <th>
                                        <div class="btn-group dropdown">
                                          <button type="submit" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Modify</button>
                                          <ul class="dropdown-menu">
                                            <li>{!! Form::submit( 'Delete', ['class' => 'btn btn-default', 'name' => 'submit', 'value' => 'delete'])!!}</li>
                                            <li>{!! Form::submit( 'Update', ['class' => 'btn btn-default', 'name' => 'submit', 'value' => 'update']) !!}</li>
                                            <li>{!! Form::submit( 'Approve Users', ['class' => 'btn btn-default', 'name' => 'submit', 'value' => 'update']) !!}</li>
                                            <li>{!! Form::submit( 'Associate Subscribers', ['class' => 'btn btn-default', 'name' => 'submit', 'value' => 'update']) !!}</li>
                                            <li>{!! Form::submit( 'Associate Malls', ['class' => 'btn btn-default', 'name' => 'submit', 'value' => 'update']) !!}</li>
                                          </ul>
                                        </div>
                                    </th>
                                </tr>
                            </thead>
                        </table>
                    {!! Form::close() !!}
                    <br>
                    <!--TODO:  add must first check if has subscription. If none, don't show-->              
                    <a href="/manage/user/add"><button type="submit" class="btn btn-primary">Add User</button></a>
                </div>
            </div>
    </div>
</div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function(){
            $('.table').DataTable();
        });
    </script>
@endsection

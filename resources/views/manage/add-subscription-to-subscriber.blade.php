@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
					 <div class="navbar-container">
                        {!! Menu::get('MyNavBar')->asUl(
                            ['class' => 'nav navbar-nav nav-pills'],
                            ['class'=>'dropdown-menu']
                        ) !!}
                    </div>
				</div>

                <div class="panel-body">
                    <h2>Add Subscription</h2>

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    {{Form::open(array('action' => 'SubscriptionController@SaveItems', 'method' => 'post'))}}
                    {{ Form::hidden('_token', csrf_token() ) }}

                     <table class="table table-striped table-bordered table-hover table-sm">
                        <tbody>
                                <tr>
                                    <td>Subscriber:</td>
                                    <td>{{Form::select('subscriber', $subscribers, '',['class' => 'form-control'])}}</td>
                                </tr>
                                <tr>
                                    <td>Subscription:</td>
                                    <td>{{Form::select('subscription', $subscriptions, '',['class' => 'form-control'])}}</td>
                                </tr>
                                <!-- <tr>
                                    <td>Start Date:</td>  
                                    <td><div class="form-group">
                                        <div class='input-group date'>
                                            <input type='text' class="form-control"  name="start_date" id='datetimepicker_from' />
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div></td>
                                </tr>
                                <tr>
                                    <td>End Date:</td>
                                    <td><div class="form-group">
                                        <div class='input-group date'>
                                            <input type='text' class="form-control" name="end_date" id='datetimepicker_to' />
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div></td>
                                </tr> -->
                                <tr>
                                    <td>Advertising Credit(s) (days):</td>
                                    <td>{{ Form::number('advertising_credit', null ,array('class' => 'form-control')) }}</td>
                                </tr>
                                <tr>
                                    <td>Remarks:</td>
                                    <td>{{ Form::textarea('remarks', null ,array('class' => 'form-control')) }}</td>
                                </tr>
                        </tbody>
                    </table>

                    <button type="submit" class="btn btn-primary">Submit</button>
                    {!! Form::close() !!}
                     <br>
                     <a href="/manage/subscriber"> <button type="submit" class="btn btn-primary">View Subscribers</button></a>
                     <br><br>
                      <a href="/manage/subscription"> <button type="submit" class="btn btn-primary">View Subscriptions</button></a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script type="text/javascript">

         $(document).ready(function(){
           $('#datetimepicker_from').datetimepicker({ format: 'LLL'});
           $('#datetimepicker_to').datetimepicker({ format: 'LLL'});
        });

    </script>
@endsection
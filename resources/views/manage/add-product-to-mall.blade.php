@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                     <div class="navbar-container">
                        {!! Menu::get('MyNavBar')->asUl(
                            ['class' => 'nav navbar-nav nav-pills'],
                            ['class'=>'dropdown-menu']
                        ) !!}
                    </div>
                </div>

                <div class="panel-body">
                    <h2>Associate Mall to Product</h2>

                    @if($errors->any())
                        <div class="alert alert-danger">
                          <strong>Warning!</strong> {{$errors->first()}}
                        </div>
                    @endif

                    
                    <div class="alert alert-info">
                        <strong>Associated malls with {{ $items[0]->title}}: </strong> 
                        @foreach($mallproduct as $mall)
                            {{$mall->mall->name}},
                        @endforeach
                    </div>

                    @if(count($mallproduct) > 0)
                    
                    <a href="/manage/product/remove/mall/{{$mallproduct[0]->product_id}} "> <button type="submit" class="btn btn-primary">Remove Associated Mall(s)</button></a>
                    <br><br>
                    @endif
                    
                    
                    <div class="alert alert-info info-indicator">
                        <strong>Mall(s) to be added: </strong>
                    </div>

                    {{Form::open(array('action' => 'ProductController@AddProductToMall', 'method' => 'post'))}}
                    {{ Form::hidden('_token', csrf_token() ) }}

                    @if(count($items) > 0)
                     <table class="table table-striped table-bordered table-hover table-sm">
                       <tbody>
                            @foreach ($items as $item)
                                {{ Form::hidden('product_id', $item->id ) }}
                                <tr>
                                    <td>Shop:</td>
                                    <td>{{$item->Shop['title']}}</td>
                                </tr>
                                <tr>
                                    <td>Product:</td>
                                    <td>{{$item->title}}</td>
                                </tr>
                                 <tr> 
                                    <td>Mall:</td>  
                                    <td>
                                       @foreach ($malls as $mall)                    
                                        <div class="form-check">
                                          <label class="form-check-label">
                                            <input class="form-check-input" type="checkbox" name="malls[]" value="{{$mall->id}}"" title="{{$mall->name}}">
                                                {{$mall->name}}
                                          </label>
                                        </div>   
                                        @endforeach 
                                        <!--TODO: checkbox must be checked if $item->id in $shop->id  -->
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @endif
                    <br>
                    <button type="submit" class="btn btn-primary">Submit</button>
                    {!! Form::close() !!}
                     <br>
                     <a href="/manage/product"> <button type="submit" class="btn btn-primary">View Products</button></a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $( ":checkbox" ).change(function(){
            var title = $(this).attr('title');
            var id = this.value;
            if(this.checked){
                $('.info-indicator').append('<span id="' + id +'"> ' +  title +  ', </span>');
            }else{
                 $('#' + id).remove();
            }

        });
    </script>
@endsection
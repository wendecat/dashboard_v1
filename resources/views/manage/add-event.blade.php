@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
					 <div class="navbar-container">
                        {!! Menu::get('MyNavBar')->asUl(
                            ['class' => 'nav navbar-nav nav-pills'],
                            ['class'=>'dropdown-menu']
                        ) !!}
                    </div>
				</div>

                <div class="panel-body">
                    <h2>Add Event</h2>

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    {{Form::open(array('action' => 'EventController@SaveItems', 'method' => 'post','files' => true))}}
                         {{ Form::hidden('_token', csrf_token() ) }}
                     <table class="table table-striped table-bordered table-hover table-sm">
                        <tbody>
                            <tr>
                                <td>Title:</td>
                                <td>{{ Form::text('title', null ,array('class' => 'form-control')) }}</td>
                            </tr>
                            <tr>
                                <td>Description:</td>
                                <td>{{ Form::textarea('description', null, array('class' => 'form-control')) }}</td>
                            </tr>
                            <tr> 
                                    <td>Mall:</td>  
                                    <td>
                                       @foreach ($malls as $mall)                    
                                        <div class="form-check">
                                          <label class="form-check-label">
                                            <input class="form-check-input" type="checkbox" name="malls[]" value="{{$mall->id}}">
                                                {{$mall->name}}
                                          </label>
                                        </div>   
                                        @endforeach 
                                        <!--TODO: checkbox must be checked if $item->id in $shop->id  -->
                                    </td>
                                </tr>
                            <tr>
                                <td>Show From:</td>
                                <td><div class="form-group">
                                    <div class='input-group date'>
                                        <input type='text' class="form-control"  name="show_from" id='datetimepicker_from' />
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div></td>
                            </tr>
                            <tr>
                                <td>Show To:</td>
                                <td><div class="form-group">
                                    <div class='input-group date'>
                                        <input type='text' class="form-control" name="show_to" id='datetimepicker_to' />
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div></td>
                            </tr>
                            <tr>
                                <td>Image:</td>
                                <td> {{ Form::file('image') }}</td>
                            </tr>
                        </tbody>
                    </table>
                    <a href="/manage/event/add"><button type="submit" class="btn btn-primary">Submit</button></a>
                    
                     {!! Form::close() !!}
                     <br>
                     <a href="/manage/event"> <button type="submit" class="btn btn-primary">View Events</button></a>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function(){
           $('#datetimepicker_from').datetimepicker({ format: 'LLL'});
           $('#datetimepicker_to').datetimepicker({ format: 'LLL'});
        });
</script>
@endsection

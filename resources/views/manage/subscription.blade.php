@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                     <div class="navbar-container">
                        {!! Menu::get('MyNavBar')->asUl(
                            ['class' => 'nav navbar-nav nav-pills'],
                            ['class'=>'dropdown-menu']
                        ) !!}
                    </div>
                </div>

                <div class="panel-body">
                    <h2>Subscriptions</h2>

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    {{Form::open(array('action' => 'SubscriptionController@UpdateItems', 'method' => 'get'))}}
                        {{ Form::hidden('_token', csrf_token() ) }}
                    @if(count($items) > 0)
                     <table class="table table-striped table-bordered table-hover table-sm">
                       <thead>
                            <tr>
                                <th>#</th>
                                <th>Subscriber</th>
                                <th>Subscription</th>
                                <!-- <th>Start Date</th> -->
                                <!-- <th>End Date</th> -->
                                <th>Adversting Credit(s)</th>
                                <th>Remaining Credit(s)</th>
                                <th>Remarks</th>
                                <th>
                                    <div class="btn-group dropdown">
                                      <button type="submit" class="btn btn-primary dropdown-toggle glyphicon glyphicon-menu-hamburger" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                                      <ul class="dropdown-menu">
                                        <li>{!! Form::submit( 'Delete', ['class' => 'btn btn-primary', 'name' => 'submit'])!!}</li>
                                        <li>{!! Form::submit( 'Update', ['class' => 'btn btn-primary', 'name' => 'submit']) !!}</li>
                                        <li>{!! Form::submit( 'View Details', ['class' => 'btn btn-primary', 'name' => 'submit']) !!}</li>
                                      </ul>
                                    </div>
                                </th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach ($items as $item)
                                <tr class="{{$item->subscriber->subscriber_code}} {{$item->subscription->subscription_code}}">
                                    <th scope="row">{{$loop->iteration}}</th>
                                    <td>{{$item->subscriber->name}} </td>
                                    <td>{{$item->subscription->name}}</td>
                                    <!-- <td>{{--$item->start_date--}}</td> -->
                                    <!-- <td>{{--$item->end_date--}}</td> -->
                                    <td>{{$item->advertising_credit}}</td>
                                    <td></td><!-- remaining credit -->
                                    <td>{{$item->remarks}}</td>
                                    <td>{{ Form::checkbox('selected[]',$item->id) }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Subscriber</th>
                                <th>Subscription</th>
                                <!-- <th>Start Date</th> -->
                                <!-- <th>End Date</th> -->
                                <th>Adversting Credit(s)</th>
                                <th>Remaining Credit(s)</th>
                                <th>Remarks</th>
                                <th>
                                    <div class="btn-group dropdown">
                                       <button type="submit" class="btn btn-primary dropdown-toggle glyphicon glyphicon-menu-hamburger" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                                      <ul class="dropdown-menu">
                                        <li>{!! Form::submit( 'Delete', ['class' => 'btn btn-primary', 'name' => 'submit'])!!}</li>
                                        <li>{!! Form::submit( 'Update', ['class' => 'btn btn-primary', 'name' => 'submit']) !!}</li>
                                        <li>{!! Form::submit( 'View Details', ['class' => 'btn btn-primary', 'name' => 'submit']) !!}</li>
                                      </ul>
                                    </div>
                                </th>
                            </tr>
                        </thead>
                    </table>
                    @else
                        <div class="card">
                          <div class="card-block text-nowrap">
                            Oops! It seems there are no subscriptions.
                          </div>
                        </div>
                    @endif
                    {!! Form::close() !!}
                    <br>
                    <a href="/manage/subscription/add"><button type="submit" class="btn btn-primary">Add</button></a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script type="text/javascript">

        $( document ).ready(function() {
           
        });

    </script>
@endsection

<!--
    TODO: group showing of items based on subscriber, subscription
    TODO: compute total based on subscriber, subscription
    TODO: show items based on multiple select values
-->
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
					 <div class="navbar-container">
                        {!! Menu::get('MyNavBar')->asUl(
                            ['class' => 'nav navbar-nav nav-pills'],
                            ['class'=>'dropdown-menu']
                        ) !!}
                    </div>
				</div>

                <div class="panel-body">
                    <h2>Associate Shops</h2>
                    
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <div class="alert alert-info">
                        <strong>Associated shops with {{ $subscriber[0]->name }}: </strong>
                        @foreach($shops as $shop)
                            {{$shop->shop->title}},
                        @endforeach
                    </div>

                    <a href="/manage/subscriber/remove/shop/{{$subscriber[0]->id}} "> <button type="submit" class="btn btn-primary">Remove Associated Shops</button></a>
                    <br><br>
                    <div class="alert alert-info info-indicator">
                        <strong>Shop(s) to be added: </strong>
                    </div>

                    {{Form::open(array('action' => 'SubscriberController@AddShop', 'method' => 'post'))}}
                    {{ Form::hidden('_token', csrf_token() ) }}
                    {{ Form::hidden('subscriber_id', $subscriber[0]->id ) }}

                    @if(count($items) > 0)
                     <table class="table table-striped table-bordered table-hover table-sm">
                       <thead>
                            <tr>
                                <th>#</th>
                                <th>Title</th> 
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($items as $item)
                                <!-- <tr class="table-tr" data-url="/manage/subscriber/add/shop/{{$item->id}}"> -->
                                <tr>
                                    <th scope="row">{{$loop->iteration}}</th>
                                    <td>{{$item->title}}</td>
                                    <td><input type="checkbox" name="selected[]" value="{{$item->id}}" title="{{$item->title}}" /></td><!--TODO: checkbox must be checked if $item->id in $shop->id  -->
                                </tr>
                            @endforeach
                        </tbody>
                        <thead>
                           <tr>
                                <th>#</th>
                                <th>Title</th>
                                <th></th>
                            </tr>
                        </thead>
                    </table>
                    @endif
                    <br>
                    <button type="submit" class="btn btn-primary">Submit</button>
                    {!! Form::close() !!}
                     <br>
                     <a href="/manage/subscriber"> <button type="submit" class="btn btn-primary">View Subscribers</button></a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function(){
            $('.table').DataTable();
        });

        $( ":checkbox" ).change(function(){
            var title = $(this).attr('title');
            var id = this.value;
            if(this.checked){
                $('.info-indicator').append('<span id="' + id +'"> ' +  title +  ', </span>');
            }else{
                 $('#' + id).remove();
            }

        });
        
        $(function () {
            // $(".table").on("click", "tr[data-url]", function () {
            //     var url = $(this).attr('data-url');
            //     $.ajax({
            //         url: url,
            //         type: 'GET',
            //          success:function(data) {
            //             document.location = url;
            //         },
            //     });
            // });
        });
    </script>
@endsection
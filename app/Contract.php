<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contract extends Model
{
	/*************************************************************************************************
	*	Description: contains the subscription type and subscribed air time
	*	of subscriber. will use the air time in this model for checking
	*	available running air time of subscriber's subscription.
	*
	**************************************************************************************************/

    public function Subscriber(){
        return $this->belongsTo('App\Subscriber');
    }

    public function Subscription(){
        return $this->belongsTo('App\Subscription');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MallProduct extends Model
{
    protected $table = 'malls_products';

    public function Mall(){
    	return $this->belongsTo('App\Mall');
    }

    public function Product(){
    	return $this->belongsTo('App\Mall');
    }
}

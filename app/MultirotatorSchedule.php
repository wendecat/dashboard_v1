<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MultirotatorSchedule extends Model
{
    protected $table = 'multirotatorschedules';

    public function Multirotator(){
        return $this->belongsTo('App\Multirotator');
    }

    public function Mall(){
    	return $this->belongsTo('App\Mall');
    }

     public function Subscriber(){
    	return $this->belongsTo('App\Subscriber');
    }
}

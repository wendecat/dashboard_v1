<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Multirotator extends Model
{
    protected $table = 'multirotators';

    public function MultirotatorSchedule(){
       return $this->hasmany('App\MultirotatorSchedule');
    }

}

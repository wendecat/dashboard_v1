<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shop extends Model
{
	protected $table = 'shops';
	
    public function ShopSubscriber(){
    	return $this->hasMany('App\ShopSubscriber');
    }

    public function CategoryShop(){
    	return $this->hasMany('App\CategoryShop');
    }

    public function ShopLogo(){
    	return $this->belongsTo('App\ShopLogo');
    }

    public function MallShop(){
        return $this->hasMany('App\MallShop');
    }

    public function Product(){
        return $this->hasMany('App\Product');
    }

}

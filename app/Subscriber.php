<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscriber extends Model
{
    protected $table = 'subscribers';

    public function SubscriberSubscription(){
        return $this->hasMany('App\SubscriberSubscription');
    }

    public function SubscriberUser(){
        return $this->hasMany('App\SubscriberUser');
    }

    public function Contract(){
        return $this->hasMany('App\Contract');
    }

    public function Multirotator(){
        return $this->hasMany('App\MultirotatorSchedule');
    }

     public function ShopSubscriber(){
        return $this->hasMany('App\ShopSubscriber');
    }
}

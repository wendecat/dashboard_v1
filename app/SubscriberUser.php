<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubscriberUser extends Model
{
    protected $table = 'subscribers_users';
    protected $fillable = array('subscriber_id',);

    public function User(){
        return $this->belongsTo('App\User');
    }

    public function Subscriber(){
        return $this->belongsTo('App\Subscriber');
    }
}

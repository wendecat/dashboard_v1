<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShopLogo extends Model
{
	protected $table = 'shops_logo';

    public function Shop(){
    	return $this->hasOne('App\Shop');
    }
}

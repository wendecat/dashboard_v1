<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryShop extends Model
{
    protected $table = 'categories_shops';

    public function Shop(){
    	return $this->belongsTo('App\Shop');
    }

    public function Category(){
    	return $this->belongsTo('App\Category');
    }

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShopSubscriber extends Model
{
    protected $table = 'shops_subscribers';
    //protected $fillable = [];
    //protected $guarded = ['shop_id','subscriber_id'];

    public function Shop(){
    	return $this->belongsTo('App\Shop');
    }

    public function Subscriber(){
    	return $this->belongsTo('App\Subscriber');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

	//protected $fillable = array('product_id','mall_id','is_active','shop_id');

    public function Shop(){
    	return $this->belongsTo('App\Shop');
    }
}

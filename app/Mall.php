<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mall extends Model
{
    public function MultirotatorSchedule(){
    	return $this->hasMany('App\MultirotatorSchedule');
    }

    public function Event(){
    	return $this->hasMany('App\Event');
    }

    public function MallUser(){
    	return $this->hasMany('App\MallUser');
    }

    public function Kiosk(){
    	return $this->hasMany('App\Kiosk');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MallShop extends Model
{
    protected $table = 'malls_shops';

    public function Mall(){
    	return $this->belongsTo('App\Mall');
    }

    public function Shop(){
    	return $this->belongsTo('App\Shop');
    }
    
}

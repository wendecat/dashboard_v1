<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubscriberSubscription extends Model
{
    protected $table = 'subscribers_subscriptions';

    public function Subscriber()
    {
        return $this->belongsTo('App\Subscriber');
    }

    public function Subscription(){
        return $this->belongsTo('App\Subscription');
    }
}

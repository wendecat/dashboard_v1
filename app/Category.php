<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function Group(){
    	return $this->belongsTo('App\Group');
    }

    public function CategoryShop(){
    	return $this->hasMany('App\CategoryShop');
    }
}

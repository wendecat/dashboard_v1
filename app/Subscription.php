<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    protected $table = 'subscriptions';

    public function SubscriberSubscription(){
        return $this->hasMany('App\SubscriberSubscription');
    }

    public function Contract(){
        return $this->hasMany('App\Contract');
    }
}

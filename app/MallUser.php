<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MallUser extends Model
{
    protected $table = 'malls_users';
    protected $fillable = array('user_id',);

    public function Mall(){
    	return $this->belongsTo('App\Mall');
    }

    public function User(){
    	return $this->belongsTo('App\User');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\RoleUser;
use App\Category;
use App\Group;
use Carbon\Carbon;
use Validator;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getRole(){

        //get id of logged in user
        $user_id = Auth::User()->id;
        $role_id = array();
        //get role of user. 1 => admin, 2 => subscriber
       
        $roles = RoleUser::where('user_id', '=', $user_id)->get();
        foreach ($roles as $role) {
            array_push($role_id, $role->role_id);
        }

        return $role_id;
    }

    public function ValidateFormInput($request){
        return $this->validate($request, [
            'name' => 'required',
            'group' => 'required',
            'is_active' => 'required',
        ]);
    }

    public function ValidateFormArray($request){
        return $this->validate($request, [
            'name[]' => 'required',
            'group[]' => 'required',
            'is_active[]' => 'required',
        ]);
    }

    public function ShowItems(){

        $user_id = Auth::User()->id;
        $role_id = $this->getRole();

        if (in_array(1, $role_id)) { //pemi
    	
    	$items = Category::with('Group')->where('is_active','=',1)->orderBy('name')->get();
    	return view('.manage.category',['items' => $items]);
    	//return $items;

        } else {
            return view('no-permissions');
        }
    	
    }

    public function UpdateItems(Request $request){
    	$user_id = Auth::User()->id;
        $input = $request->input('selected');
        $count = count($input);

        switch($request->submit) {
            case 'Delete': 
                Category::whereIn('id', $input)->update(['is_active' => 0, 'deleted_by' => $user_id]);
                return redirect()->action('CategoryController@ShowItems');

            case 'Update': 
               	$group = Group::where('is_active','=',1)->orderBy('name')->pluck('name','id')->reverse()->put('', '-----')->reverse();
                $items = Category::whereIn('id', $input)->get();
                return view('.manage.update-category', ['items'=> $items, 'group' => $group]);

            break;
        }
    }

    public function AddItems(){
    	$group = Group::where('is_active','=',1)->orderBy('name')->pluck('name','id')->reverse()->put('', '-----')->reverse();
    	return view('manage.add-category', ['group' => $group]);
    }

    public function SaveItems(Request $request){

        $this->ValidateFormInput($request);

    	$name = $request->input('name');
    	$group_id = $request->input('group');
    	$is_active = $request->input('is_active');
        $created_at = Carbon::now();
        $updated_at = Carbon::now();

    	$dataSet = [
    		'name' => $name, 
    		'group_id' => $group_id,
    		'is_active' => $is_active,
    		'created_at' => $created_at,
    		'updated_at' => $updated_at,
    	];

    	Category::insert($dataSet);

    	return redirect()->action('CategoryController@ShowItems');
    }

    public function SaveUpdatedItems(Request $request){

        $this->ValidateFormArray($request);

    	$id = $request->input('id');
    	$name = $request->input('name');
    	$group_id = $request->input('group');
    	$is_active = $request->input('is_active');
        $updated_at = Carbon::now();
    	$count = count($id)-1;
        
        $i=0; //for loop counter
        for($i=0; $i <= $count; $i++){
               $dataSet[$i] = [
		    		'name' => $name[$i], 
		    		'group_id' => $group_id[$i],
		    		'is_active' => $is_active[$i],
		    		'updated_at' => $updated_at,
		    	];

            Category::where('id', '=' ,$id[$i])->update($dataSet[$i]);
        }

    	return redirect()->action('CategoryController@ShowItems');
    }
}

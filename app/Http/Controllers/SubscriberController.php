<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\RoleUser;
use App\Shop;
use App\ShopSubscriber;
use App\Subscriber;
use App\Subscription;
use App\SubscriberSubscription;
use App\SubscriberUser;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Database\QueryException;

class SubscriberController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getRole(){

        //get id of logged in user
        $user_id = Auth::User()->id;
        $role_id = array();
        //get role of user. 1 => admin, 2 => subscriber
       
        $roles = RoleUser::where('user_id', '=', $user_id)->get();
        foreach ($roles as $role) {
            array_push($role_id, $role->role_id);
        }

        return $role_id;
    }

    public function ShowItems(){ // pemi
        
        $user_id = Auth::User()->id;
        $role_id = $this->getRole();

        if (in_array(1, $role_id)) { //pemi

            $items = Subscriber::all();
            return view('manage.subscriber', ['items' => $items]);

        } else if(in_array(2, $role_id)){ //subscriber

            $subscribers = SubscriberUser::where('user_id','=',$user_id)->get();
            $subscriber_array = array();

            foreach($subscribers as $subscriber){
                array_push($subscriber_array,$subscriber->subscriber_id);
            }
            
            $items = Subscriber::whereIn('id', $subscriber_array)->where('is_active','=', 1)->get();
            return view('manage.subscriber', ['items' => $items]);

        } else  { // mall-admin

            return view('no-permissions');
        }

    }

    public function AddItems(){

        return view('manage.add-subscriber');
    }

    public function UpdateItems(Request $request){

        $user_id = Auth::User()->id;
        $input = $request->input('selected');
        $count = count($input);

        if($count < 1){
            return Redirect::back()->withErrors(['Please select item(s) to modify']);
        } else {

            switch($request->submit) {
                case 'Delete': 
                    Subscriber::whereIn('id', $input)->update(['is_active' => 0, 'deleted_by' => $user_id]);
                    return redirect()->action('SubscriberController@ShowItems');

                case 'Update': 

                    if($count > 1){
                        return Redirect::back()->withErrors(['Please select only 1 subscriber to update']);
                    } else{
                        $items = Subscriber::whereIn('id', $input)->get();
                        return view('.manage.update-subscriber', ['items'=> $items]); 
                    }

                case 'Associate Shops': 

                    if($count > 1){
                        return Redirect::back()->withErrors(['Please select only 1 subscriber to update']);
                    } else{
                        $subscriber = Subscriber::whereIn('id', $input)->get();
                        $items = Shop::all();
                        $shops = ShopSubscriber::with('Shop')->whereIn('subscriber_id', $input)->get();
                        return view('manage.add-shop-to-subscriber', ['items' => $items, 'subscriber' => $subscriber, 'shops' => $shops]);
                    }
                
                case 'Remove Shops Association':

                    if($count > 1){
                        return Redirect::back()->withErrors(['Please select only 1 subscriber to update']);
                    } else{
                         $items = ShopSubscriber::with('Shop')->where('subscriber_id','=', $input)->get();
                        return view('manage.remove-shop-from-subscriber', ['items' => $items]); 
                    }

                // case 'Add Subscription':
                //         $subscriptions =  Subscription::pluck('name','id')->reverse()->put('', '-----')->reverse();
                //         $subscriber = Subscriber::whereIn('id', $input)->get();
                //         return view('manage.add-subscription-to-subscriber', ['subscriptions' => $subscriptions, 'subscriber' => $subscriber]);

                // case 'View Subscription': 
                //         $items = SubscriberSubscription::all();
                //         return view('manage.');

                break;
            }
        }
    }

    public function SaveItems(Request $request){

        $this->validate($request, [
            'name' => 'required',
            'address' => 'required',
            'contact' => 'required',
            'email' => 'required',
        ]);

        $name =  $request->input('name');
        $address =  $request->input('address');
        $contact =  $request->input('contact');
        $email =  $request->input('email');
        $created_at = Carbon::now();
        $updated_at = Carbon::now();

        $dataSet = [
            'name' => $name,
            'address' => $address,
            'contact' => $contact,
            'email' => $email,
            'created_at' => $created_at,
            'updated_at' => $updated_at,
            'is_active' => 1
        ];
        
        Subscriber::insert($dataSet);

        return redirect()->action('SubscriberController@ShowItems');
    }

    public function SaveUpdatedItems(Request $request){

        $this->validate($request, [
            'name' => 'required',
            'address' => 'required',
            'contact' => 'required',
            'email' => 'required',
        ]);

        $id = $request->input('id');
        $name =  $request->input('name');
        $address =  $request->input('address');
        $contact =  $request->input('contact');
        $email =  $request->input('email');
        $updated_at = Carbon::now();
        
        /*$count = count($ids)-1;
        
        $i=0; //for loop counter
        for($i=0; $i <= $count; $i++){
                $dataSet[] = [
                'name' => $name,
                'address' => $address,
                'contact' => $contact,
                'email' => $email,
                'updated_at' => $updated_at
            ];

            Subscriber::where('id', '=' ,$ids[$i])->update($dataSet[$i]);
        }*/

        $dataSet = [
                'name' => $name,
                'address' => $address,
                'contact' => $contact,
                'email' => $email,
                'updated_at' => $updated_at
        ];

        try {
                Subscriber::where('id', '=' ,$id)->update($dataSet);
        } catch (\Illuminate\Database\QueryException $e) {
                
        } catch (PDOException $e) {
                dd($e);
        }  

        return redirect()->action('SubscriberController@ShowItems');
    }

    public function AddShop(Request $request){

        $this->validate($request, [
            'selected' => 'required',
        ]);

        $shop_id = $request->input('selected');
        $subscriber_id = $request->input('subscriber_id');
        $created_at = Carbon::now();
        $updated_at = Carbon::now();
        
        if(count($shop_id) == 1){
            
            $count = 1;
            $dataSet = [
                'shop_id' => $shop_id[0],
                'subscriber_id' => $subscriber_id,
                'created_at' => $created_at,
                'updated_at' => $updated_at, 
                'is_active' => 1
            ];

            try{
                    ShopSubscriber::insert($dataSet);
                } catch (\Illuminate\Database\QueryException $e) {
                   
                } catch (PDOException $e) {
                    
                }  

        } else {

            $count = count($shop_id)-1;
            $i=0; //for loop counter

            for($i=0; $i <= $count; $i++){
                $dataSet[$i] = [
                'shop_id' => $shop_id[$i],
                'subscriber_id' => $subscriber_id,
                'created_at' => $created_at,
                'updated_at' => $updated_at, 
                'is_active' => 1
            ];

                try{
                    ShopSubscriber::insert($dataSet[$i]);
                } catch (\Illuminate\Database\QueryException $e) {
                   
                } catch (PDOException $e) {
                    
                }  
            }
        }
             
        //TODO: must return to add shops pages. I'm getting route exception when doing return Redirect::back()
        return redirect()->action('SubscriberController@ShowItems');
        //return $count;
    }

    public function RemoveShop($id){

         $items = ShopSubscriber::with('Shop')->where('subscriber_id','=', $id)->where('is_active','=','1')->get();
         return view('manage.remove-shop-from-subscriber', ['items' => $items]); 

    }

    public function SaveRemovedShop(Request $request){

        $this->validate($request, [
            'selected' => 'required',
        ]);

        $user_id = Auth::User()->id;
        $input = $request->input('selected');
        $subscriber_id = $request->input('subscriber_id');
        
        //ShopSubscriber::whereIn('shop_id', $input)->where('subscriber_id','=',$subscriber_id)->update(['is_active' => 0, 'deleted_by' => $user_id]);
        
        // Totally remove deleted items
        ShopSubscriber::whereIn('shop_id', $input)->where('subscriber_id','=',$subscriber_id)->delete();

        return redirect()->action('SubscriberController@ShowItems');

    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\RoleUser;	
use App\Mall;
use App\MallShop;
use App\Shop;
use Carbon\Carbon;

class MallShopController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getRole(){

        //get id of logged in user
        $user_id = Auth::User()->id;
        $role_id = array();
        //get role of user. 1 => admin, 2 => subscriber
       
        $roles = RoleUser::where('user_id', '=', $user_id)->get();
        foreach ($roles as $role) {
            array_push($role_id, $role->role_id);
        }

        return $role_id;
    }

    public function ValidateFormInput($request){
        return $this->validate($request, [
            'shop' => 'required',
            'malls' => 'required',
            'contact' => 'required',
            'rank' => 'required',
            'description' => 'required',
            'email' => 'required',
            'space_number' => 'required',
            'location_description' => 'required',
            'is_active' => 'required',
        ]);
    }

    public function ShowItems(){

        $user_id = Auth::User()->id;
        $role_id = $this->getRole();

            if (in_array(1, $role_id) || in_array(3, $role_id)) { //pemi || mall-admin
        	   $items = MallShop::with('Mall','Shop')->get();
        	   return view('.manage.mallshop', ['items' => $items]);
               //return $items;
        	//return count($items);
            } else {
                return view('no-permissions');
            }   
    }

    public function UpdateItems(Request $request){
    	$input = $request->input('selected');
    	$user_id = Auth::User()->id;
        $role_id = $this->getRole();
        $count = count($input);

        switch($request->submit) {
    		case 'Delete': 
              
              MallShop::whereIn('id', $input)->update(['is_active' => 0, 'deleted_by' => $user_id]);

              return redirect()->action('MallShopController@ShowItems');

 
            case 'Update': 

                if($count > 1){

                    return Redirect::back()->withErrors(['Please select only 1 item to update']);

                } else {

    	            if (in_array(1, $role_id) || in_array(3, $role_id)) { //pemi || mall-admin
    	            	$shops = Shop::where('is_active','=',1)->orderBy('title')->pluck('title','id')->reverse()->put('', '-----')->reverse();
    	            	$malls = Mall::pluck('name','id')->reverse()->put('', '-----')->reverse();
    		    	  	$items = MallShop::with('Mall','Shop')->whereIn('id',$input)->get();
                    	return view('.manage.update-mallshop', ['items'=> $items, 'malls' => $malls, 'shops' => $shops]);
                    	//return $items;

    		        } else { //subscriber
                  
                	}
            }
        }
    }

    public function SaveUpdatedItems(Request $request){

        $this->ValidateFormInput($request);

    	$id = $request->input('id');
    	$shop_id = $request->input('shop');
    	$mall_id = $request->input('malls');
    	$contact = $request->input('contact');
    	$rank = $request->input('rank');
    	$description = $request->input('description');
    	$email = $request->input('email');
    	$space_number = $request->input('space_number');
    	$location_description = $request->input('location_description');
    	$is_active = $request->input('is_active');
    	$updated_at = Carbon::now();
    	
        /*$count = count($id) - 1;

    	$i=0; //for loop counter
        for($i=0; $i <= $count; $i++){
            $dataSet[] = [
                
                'mall_id' => $mall_id[$i],
                //map_id
                'shop_id' => $shop_id[$i],
                'is_active' => $is_active[$i],
                'contact' => $contact[$i],
                'rank' => $rank[$i],
                'description' => $description[$i],
                'email' => $email[$i],
                'space_number' => $space_number[$i],
                'location_description' => $location_description[$i],
                'updated_at' => $updated_at
            ];

            MallShop::where('id', '=' ,$id[$i])->update($dataSet[$i]);
        }*/

         $dataSet = [
                
                'mall_id' => $mall_id,
                //map_id
                'shop_id' => $shop_id,
                'is_active' => $is_active,
                'contact' => $contact,
                'rank' => $rank,
                'description' => $description,
                'email' => $email,
                'space_number' => $space_number,
                'location_description' => $location_description,
                'updated_at' => $updated_at
            ];

            MallShop::where('id', '=' ,$id)->update($dataSet);
      
        return redirect()->action('MallShopController@ShowItems');
    }

    public function AddItems(){
    	
    	$shops = Shop::where('is_active','=',1)->orderBy('title')->pluck('title','id')->reverse()->put('', '-----')->reverse();
	    $malls = Mall::pluck('name','id')->reverse()->put('', '-----')->reverse();

	    return view('manage.add-mallshop', ['shops' => $shops, 'malls' => $malls]);
    }

    public function SaveItems(Request $request){

        $this->ValidateFormInput($request);

        $id = $request->input('id');
    	$shop_id = $request->input('shop');
    	$mall_id = $request->input('malls');
    	$contact = $request->input('contact');
    	$rank = $request->input('rank');
    	$description = $request->input('description');
    	$email = $request->input('email');
    	$space_number = $request->input('space_number');
    	$location_description = $request->input('location_description');
    	$is_active = $request->input('is_active');
    	$created_at = Carbon::now();
    	$updated_at = Carbon::now();
   
        $dataSet = [
            'mall_id' => $mall_id,
            //map_id
            'shop_id' => $shop_id,
            'is_active' => $is_active,
            'contact' => $contact,
            'rank' => $rank,
            'description' => $description,
            'email' => $email,
            'space_number' => $space_number,
            'location_description' => $location_description,
            'created_at' => $created_at,
            'updated_at' => $updated_at,
        ];

        MallShop::insert($dataSet);
        
      
        return redirect()->action('MallShopController@ShowItems');	

    }
}

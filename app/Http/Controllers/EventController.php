<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\UploadedFile;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Redirect;
use App\User;
use App\RoleUser;
use App\Event;
use App\Mall;
use App\MallUser;
use Carbon\Carbon;

class EventController extends Controller
{

	protected $request;

	public function __construct()
    {
        $this->middleware('auth');  
    }

    public function getRole(){

        //get id of logged in user
        $user_id = Auth::User()->id;
        $role_id = array();
        //get role of user. 1 => admin, 2 => subscriber
       
        $roles = RoleUser::where('user_id', '=', $user_id)->get();
        foreach ($roles as $role) {
            array_push($role_id, $role->role_id);
        }

        return $role_id;
    }

    public function ValidateFormInput($request){
        return $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'malls' => 'required',
            'show_from' => 'required',
            'show_to' => 'required',
            'image' => 'required',
        ]);
    }

    public function ValidateFormInputNoImage($request){
        return $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'malls' => 'required',
            'show_from' => 'required',
            'show_to' => 'required',
        ]);
    }

    public function ShowItems(){
        
        $user_id = Auth::User()->id;
        $role_id = $this->getRole();

        if (in_array(1, $role_id)) { //pemi
    	   $items = Event::with('Mall')->where('is_active', '=', 1)->get();
    	   return view('.manage.event')->with('items', $items);
        } elseif(in_array(3, $role_id)) { //mall-admin
            //check malls associated with user
            $malls = MallUser::with('Mall')->where('user_id', '=', $user_id)->get();
            $mall_id = array();
            
            foreach($malls as $mall){
               array_push($mall_id , $mall->mall_id);
            }
            
            $items = Event::whereIn('mall_id', $mall_id)->where('is_active', '=', 1)->get();
            return view('.manage.event')->with('items', $items);
            //return $mall_id;
        } else {
            return view('no-permissions');
        }
    }

    public function UpdateItems(Request $request){

        $user_id = Auth::User()->id;
        $role_id = $this->getRole();

        $malls = array();
        $input = $request->input('selected');

        $count = count($input);
    
        switch($request->submit) {

            case 'Delete': 

                Event::whereIn('id', $input)->update(['is_active' => 0, 'deleted_by' => $user_id]);

                return redirect()->action('EventController@ShowItems');

            case 'Update': 

                if($count > 1){
                    
                    return Redirect::back()->withErrors(['Please select only 1 item to update']);

                } else {
               
                    $items = Event::with('Mall')->whereIn('id', $input)->get();

                    if (in_array(1, $role_id)) { //admin
                        //get all malls
                        $malls = Mall::pluck('name','id')->reverse()->put('', '-----')->reverse();
                        //$malls = Mall::all();

                    }else { //non-admin
                        //get all malls associated with the user
                         $malls = MallUser::where('user_id','=',$user_id)->join('malls', 'malls_users.mall_id','=', 'malls.id')->pluck('malls.name','mall_id')->reverse()->put('', '-----')->reverse();
                    }

                    return view('.manage.update-event', array('items'=> $items, 'malls' => $malls));  
                }  

            break;
        }

    }

    public function SaveUpdatedItems(Request $request){

        $this->ValidateFormInputNoImage($request);
        
        $id = $request->input('id');
        $mall_id =  $request->input('malls');
        $title =  $request->input('title');
        $description =  $request->input('description');
        
        $show_from = $request->input('show_from');
        $show_to = $request->input('show_to');
        
        //removed updating of multiples items at once
        /*$count = count($id)-1;
        
        $i=0; //for loop counter
        for($i=0; $i <= $count; $i++){
            $dataSet[] = [
                'mall_id' => $mall_id[$i],
                'title' => $title[$i],
                'description' => $description[$i],
                'show_from' => date('Y-m-d H:i:s', strtotime($show_from[$i])),
                'show_to' => date('Y-m-d H:i:s', strtotime($show_to[$i])),
            ];

            Event::where('id', '=' ,$ids[$i])->update($dataSet[$i]);
        }*/

        $dataSet = [
            'mall_id' => $mall_id,
            'title' => $title,
            'description' => $description,
            'show_from' => date('Y-m-d H:i:s', strtotime($show_from)),
            'show_to' => date('Y-m-d H:i:s', strtotime($show_to)),
        ];

        Event::where('id', '=' ,$id)->update($dataSet);
      
        return redirect()->action('EventController@ShowItems');
    }

    public function AddItems(){
        
        $user_id = Auth::User()->id;
        $role_id = $this->getRole();

        $malls = array();
        if (in_array(1, $role_id)) { //admin
            //get all malls
            // $malls = Mall::pluck('name','id')->reverse()->put('', '-----')->reverse();
            $malls = Mall::orderBy('name')->get();

        }else { //non-admin
            //get all malls associated with the user
            //$malls = MallUser::where('user_id','=',$user_id)->join('malls', 'malls_users.mall_id','=', 'malls.id')->pluck('malls.name','mall_id')->reverse()->put('', '-----')->reverse();
            $malls = MallUser::where('user_id','=',$user_id)->join('malls', 'malls_users.mall_id','=', 'malls.id');
        }
        return view('.manage.add-event')->with('malls', $malls);
    }

    public function SaveItems(Request $request){

        $this->ValidateFormInput($request);

        $mall_id =  $request->input('malls');
    	$title =  $request->input('title');
        $description =  $request->input('description');
        
        $show_from =  date('Y-m-d H:i:s', strtotime($request->input('show_from')));
        $show_to =  date('Y-m-d H:i:s', strtotime($request->input('show_to')));
        $image =  $request->input('image');

        $path = './public/images/event';
        $md5Name = md5_file($request->file('image')->getRealPath());
        $guessExtension = $request->file('image')->guessExtension();
        $file = $request->file('image')->storeAs($path, $md5Name.'.'.$guessExtension);
        $created_at = Carbon::now();
        $updated_at = Carbon::now();
        $count = count($mall_id) - 1;

        $i=0; //for loop counter
        for($i=0; $i <= $count; $i++){
               $dataSet[$i] = [
                    'mall_id' => $mall_id[$i],
                    'title' => $title,
                    'description' => $description,
                    'show_from' => $show_from,
                    'show_to' => $show_to, 
                    'image' => $md5Name.'.'.$guessExtension,
                    'created_at' => $created_at,
                    'updated_at' => $updated_at
                ];
            
                try{
                    Event::insert($dataSet[$i]);
                } catch (\Illuminate\Database\QueryException $e) {
                   //action here
                } catch (PDOException $e) {
                    //action here
                }  
        }
        
        return redirect()->action('EventController@ShowItems'); 
        //return $count;
    }
}

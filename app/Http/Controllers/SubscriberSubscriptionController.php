<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Subscriber;
use App\Subscription;
use App\SubscriberSubscription;

class SubscriberSubscriptionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }




    public function showItems(){
        //TODO: get only items where subscriber id is logged in
        $items = SubscriberSubscription::with('Subscriber')->where('subscriber_id', '=' ,1)->get();
        return $items;
        //return view('multirotator')->with('items', $items);
    }
}

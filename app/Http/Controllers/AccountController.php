<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use App\Account;

class AccountController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }

     public function showItems(){
    	
    	return view('.manage.admin-accounts');
    }
}

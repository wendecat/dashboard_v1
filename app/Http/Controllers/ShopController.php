<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\User;
use App\Shop;
use App\Category;
use App\CategoryShop;
use App\RoleUser;
use App\ShopLogo;
use Carbon\Carbon;

class ShopController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }

    public function getRole(){

        //get id of logged in user
        $user_id = Auth::User()->id;
        $role_id = array();
        //get role of user. 1 => admin, 2 => subscriber
       
        $roles = RoleUser::where('user_id', '=', $user_id)->get();
        foreach ($roles as $role) {
            array_push($role_id, $role->role_id);
        }

        return $role_id;
    }

    public function ValidateFormInput($request){
        return $this->validate($request, [
            'title' => 'required',
            'image' => 'required',
            'category' => 'required',
            'is_active' => 'required',
        ]);
    }

    public function ShowItems(){

        $user_id = Auth::User()->id;
        $role_id = $this->getRole();

        if (in_array(1, $role_id) || in_array(3, $role_id)) { //pemi || mall-admin
    	   $items = Shop::with('ShopLogo')->get();
    	   return view('.manage.shop', ['items' => $items]);
    	   //return $items;
        } else {
            return view('no-permissions');
        }
    }

    public function UpdateItems(Request $request){
    	
    	$input = $request->input('selected');
    	$user_id = Auth::User()->id;
        $role_id = $this->getRole();
        $count = count($input);

         switch($request->submit) {

            case 'Delete': 
              
              Shop::whereIn('id', $input)->update(['is_active' => 0, 'deleted_by' => $user_id]);

              return redirect()->action('ShopController@ShowItems');

            case 'Update': 

                if($count > 1){

                    return Redirect::back()->withErrors(['Please select only 1 item to update']);
                    
                } else {
                    if (in_array(1, $role_id) || in_array(3, $role_id)) { //pemi || mall-admin

                        $logo = ShopLogo::orderBy('image')->pluck('image','id')->reverse()->put('', '-----')->reverse();
                        $category = Category::where('is_active','=',1)->orderBy('name')->pluck('name','id')->reverse()->put('', '-----')->reverse();
                        $items = CategoryShop::with('Category','Shop')->whereIn('id',$input)->get();
                        return view('.manage.update-shop', ['items'=> $items, 'category' => $category, 'logo' => $logo]);
                        //return $items;

                    } else { // subscribers should not be able to be here in the first place
                        return view('no-permissions');
                    }
                }
         
            break;
        }
    }

    public function AddItems(){
    	$logo = ShopLogo::orderBy('image')->pluck('image','id')->reverse()->put('', '-----')->reverse();
	    $category = Category::where('is_active','=',1)->orderBy('name')->pluck('name','id')->reverse()->put('', '-----')->reverse();
	  	//$items = CategoryShop::with('Category','Shop')->get();

	  	return view('.manage.add-shop', ['category' => $category, 'logo' => $logo]);
	  	//return $items;
    }

    public function SaveUpdatedItems(Request $request){

        $this->ValidateFormInput($request);

    	$id = $request->input('id');
    	$title = $request->input('title');
    	$shop_logo_id = $request->input('image');
    	$category = $request->input('category');
    	$is_active = $request->input('is_active');
        $updated_at = Carbon::now();
        
        /*$count = count($id) -1; 

        $i=0; //for loop counter
        for($i=0; $i <= $count; $i++){
            $dataSet[] = [
                'title' => $title[$i],
                'shop_logo_id' => $shop_logo_id[$i],
                'is_active' => $is_active[$i],
                'updated_at' => $updated_at
            ];

            Shop::where('id', '=' ,$id[$i])->update($dataSet[$i]);
        }*/

        $dataSet = [
            'title' => $title,
            'shop_logo_id' => $shop_logo_id,
            'is_active' => $is_active,
            'updated_at' => $updated_at
        ];

        Shop::where('id', '=' ,$id)->update($dataSet);
      
        return redirect()->action('ShopController@ShowItems');
    }

    public function SaveItems(Request $request){

        $this->ValidateFormInput($request);

    	$title = $request->input('title');
    	$shop_logo_id = $request->input('image');
    	$category_id = $request->input('category');
    	$is_active = $request->input('is_active');
        $created_at = Carbon::now();
        $updated_at = Carbon::now();

        $shop_id = Shop::insertGetId([
		    'title' => $title,
		    'shop_logo_id' => $shop_logo_id,
		    'is_active' => $is_active,
		    'created_at' => $created_at,
		    'updated_at' => $updated_at
        ]);

        $CategoryShop = [
        	'category_id' => $category_id,
        	'shop_id' => $shop_id,
        	'created_at' => $created_at,
		    'updated_at' => $updated_at,
		    'is_active' => 1,
        ];

        CategoryShop::insert($CategoryShop);

        return redirect()->action('ShopController@ShowItems');
    }

    
}
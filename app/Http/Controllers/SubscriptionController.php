<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\RoleUser;
use App\Subscriber;
use App\Subscription;
use App\SubscriberSubscription;
use App\SubscriberUser;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redirect;

class SubscriptionController extends Controller
{
    public function getRole(){

        //get id of logged in user
        $user_id = Auth::User()->id;
        $role_id = array();
        //get role of user. 1 => admin, 2 => subscriber
       
        $roles = RoleUser::where('user_id', '=', $user_id)->get();
        foreach ($roles as $role) {
            array_push($role_id, $role->role_id);
        }

        return $role_id;
    }

    public function ShowItems()
    {

        $user_id = Auth::User()->id;
        $role_id = $this->getRole();

        $subscriber_id = array();
        $subscriber = SubscriberUser::where('user_id', '=', $user_id)->get();
        foreach ($subscriber as $subs) {
            array_push($subscriber_id, $subs->subscriber_id);
        }

        //get list of subscriptions
        $subscriptions = Subscription::orderBy('name')->pluck('name','subscription_code')->reverse()->put('', '-----')->reverse();

        if (in_array(1, $role_id)) { //pemi

            $items =  SubscriberSubscription::where('is_active','=', 1)->with('Subscriber','Subscription')->get();
            $subscribers = Subscriber::orderBy('name')->pluck('name','id')->reverse()->put('', '-----')->reverse();

            return view('manage.subscription',array('items' => $items, 'subscribers' => $subscribers, 'subscriptions' => $subscriptions));

        } else if(in_array(2, $role_id)) { //subscriber
            
           
        } else {

            return view('no-permissions');
        }
    }

    public function AddItems(){
        $subscriptions =  Subscription::orderBy('name')->pluck('name','id')->reverse()->put('', '-----')->reverse();
        $subscribers = Subscriber::where('is_active','=',1)->orderBy('name')->pluck('name','id')->reverse()->put('', '-----')->reverse();

        return view('manage.add-subscription-to-subscriber', ['subscriptions' => $subscriptions, 'subscribers' => $subscribers]);
    }

    public function SaveItems(Request $request){

        $this->validate($request, [
            'subscriber' => 'required',
            'subscription' => 'required',
            'advertising_credit' => 'required',
            'remarks' => 'required'
        ]);

        $subscriber_id =  $request->input('subscriber');
        $subscription = $request->input('subscription');
        // $start_date = $request->input('start_date');
        // $end_date = $request->input('end_date');
        $advertising_credit = $request->input('advertising_credit');
        $remarks = $request->input('remarks');
        $created_at = Carbon::now();
        $updated_at = Carbon::now();

         $dataSet[] = [
            'subscriber_id' => $subscriber_id,
            'subscription_id' => $subscription,
            //'start_date' => date('Y-m-d H:i:s', strtotime($start_date)),
            //'end_date' => date('Y-m-d H:i:s', strtotime($end_date)),
            'advertising_credit' => $advertising_credit,
            'remarks' => $remarks,
            'created_at' => $created_at,
            'updated_at' => $updated_at,
        ];
        
        SubscriberSubscription::insert($dataSet);
        
        return redirect()->action('SubscriptionController@ShowItems');
    }

     public function UpdateItems(Request $request){

        $user_id = Auth::User()->id;
        $input = $request->input('selected');
        $count = count($input);

        if($count < 1){
            return Redirect::back()->withErrors(['Please select item(s) to modify']);
        } else {

            switch($request->submit) {

                case 'Delete': 
                    SubscriberSubscription::whereIn('id', $input)->update(['is_active' => 0, 'deleted_by' => $user_id]);
                    return redirect()->action('SubscriptionController@ShowItems');

                case 'Update': 

                    if($count < 1){

                        return Redirect::back()->withErrors(['Please select item(s) to modify']);

                    } else {

                        $items = SubscriberSubscription::whereIn('id', $input)->get();
                         $subscriptions =  Subscription::orderBy('name')->pluck('name','id')->reverse()->put('', '-----')->reverse();
                        $subscribers = Subscriber::where('is_active','=',1)->orderBy('name')->pluck('name','id')->reverse()->put('', '-----')->reverse();
                        return view('.manage.update-subscription', ['items'=> $items, 'subscribers' => $subscribers, 'subscriptions' => $subscriptions ]); 
                    }

                case 'View Details':

                    //

                break;
            }
        }
    }

    public function SaveUpdatedItems(Request $request){
        $id = $request->input('id');
        $subscriber_id =  $request->input('subscriber');
        $subscription_id =  $request->input('subscription');
        $start_date =  $request->input('start_date');
        $end_date = $request->input('end_date');
        $updated_at = Carbon::now();
    
        $dataSet = [
            'subscriber_id' => $subscriber_id,
            'subscription_id' => $subscription_id,
            'start_date' => date('Y-m-d H:i:s', strtotime($start_date)),
            'end_date' => date('Y-m-d H:i:s', strtotime($end_date)),
            'updated_at' => $updated_at
        ];

        SubscriberSubscription::where('id', '=' , $id)->update($dataSet);
      
        return redirect()->action('SubscriptionController@ShowItems');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use App\RoleUser;
use App\User;
use App\Kiosk;
use App\Mall;
use Carbon\Carbon;

class KioskController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }

    public function getRole(){

        //get id of logged in user
        $user_id = Auth::User()->id;
        $role_id = array();
        //get role of user. 1 => admin, 2 => subscriber
       
        $roles = RoleUser::where('user_id', '=', $user_id)->get();
        foreach ($roles as $role) {
            array_push($role_id, $role->role_id);
        }

        return $role_id;
    }

    public function ValidateFormInput($request){
        return $this->validate($request, [
            'mall' => 'required',
            'name' => 'required',
            'is_active' => 'required',
        ]);
    }

    public function ShowItems(){

        $user_id = Auth::User()->id;
        $role_id = $this->getRole();
    	
        if ( in_array(1, $role_id)  || in_array(3, $role_id)) { //pemi || mall-admin
    	   $items = Kiosk::with('Mall')->orderBy('mall_id','name')->get();
    	   return view('.manage.kiosk', ['items' => $items]);
    	   //return $items;
        }else { //subsriber
            return view('no-permissions');
        }
    }

    public function UpdateItems(Request $request){

    	$user_id = Auth::User()->id;
        $role_id = $this->getRole();

        $input = $request->input('selected');

        if (in_array(1, $role_id)) { //pemi
            switch($request->submit) {

                case 'Delete': 
                    Kiosk::whereIn('id', $input)->update(['is_active' => 0, 'deleted_by' => $user_id]);
                    return redirect()->action('KioskController@ShowItems');

                case 'Update': 

                    	$items = Kiosk::with('Mall')->whereIn('id', $input)->get();
                        $malls = Mall::pluck('name','id')->reverse()->put('', '-----')->reverse();
                        return view('.manage.update-kiosk', array('items'=> $items, 'malls' => $malls));          
                break;
            }

        } else { // mall-admin || subscriber
            return view('no-permissions');
        }
    }

    public function SaveUpdatedItems(Request $request){

        $this->ValidateFormInput($request);

    	$id = $request->input('id');
    	$mall_id = $request->input('mall');
    	$name = $request->input('name');
    	$is_active = $request->input('is_active');
    	$created_at = Carbon::now();
        $updated_at = Carbon::now();
        $count = count($id) -1;

        $i=0; //for loop counter
        for($i=0; $i <= $count; $i++){
	    	$dataSet[] = [
	    		'name' => $name[$i],
	    		'mall_id' => $mall_id[$i],
	    		//'map_id' => $map_id[$i],
	    		'created_at' => $created_at,
	    		'updated_at' => $updated_at,
	    		'is_active' => $is_active[$i],
	    	];

	    	Kiosk::where('id','=',$id[$i])->update($dataSet[$i]);
	    }

	    return redirect()->action('KioskController@ShowItems');
    }

    public function AddItems(){

        $user_id = Auth::User()->id;
        $role_id = $this->getRole();

        if (in_array(1, $role_id)) { //admin

    	   $malls = Mall::pluck('name','id')->reverse()->put('', '-----')->reverse();
    	   return view('manage.add-kiosk', ['malls' => $malls]);
        } else {
            return view('no-permissions');
        }

    }

    public function SaveItems(Request $request){

        $this->ValidateFormInput($request);

    	$name = $request->input('name');
    	$mall_id = $request->input('mall');
    	$is_active = $request->input('is_active');
		$created_at = Carbon::now();
        $updated_at = Carbon::now();

    	$dataSet = [
    		'name' => $name,
    		'mall_id' => $mall_id,
    		'is_active' => $is_active,
    		'created_at' => $created_at,
    		'updated_at' => $updated_at,
    	];

    	Kiosk::insert($dataSet);

    	return redirect()->action('KioskController@ShowItems');
    }
}

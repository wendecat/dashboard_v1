<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\User;
use App\RoleUser;
use App\Multirotator;
use App\MultirotatorSchedule;
use App\Mall;
use App\MallUser;
use App\Subscriber;
use App\SubscriberUser;
use App\ShopSubscriber;
use Carbon\Carbon;
use App\Shop;
use App\MallShop;
use App\SubscriberSubscription;
use App\SubscriptionLog;


class MultirotatorController extends Controller
{

	public function __construct()
    {
        $this->middleware('auth');
    }

     public function getRole(){

        //get id of logged in user
        $user_id = Auth::User()->id;
        $role_id = array();
        //get role of user. 1 => admin, 2 => subscriber
       
        $roles = RoleUser::where('user_id', '=', $user_id)->get();
        foreach ($roles as $role) {
            array_push($role_id, $role->role_id);
        }

        return $role_id;
    }

    public function ValidateFormInput($request){
        return $this->validate($request, [
            'subscriptions' => 'required',
            'name' => 'required',
            'duration' => 'required',
            'malls' => 'required',
            'subscribers' => 'required',
            'shops' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
            'image' => 'required'
        ]);
    }

    public function ValidateFormInputNoImage($request){
        return $this->validate($request, [
            'subscriptions' => 'required',
            'name' => 'required',
            'duration' => 'required',
            'malls' => 'required',
            'subscribers' => 'required',
            'shops' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
        ]);
    }
	
    public function ShowItems(){
        
        $user_id = Auth::User()->id;
        $role_id = $this->getRole();

        if (in_array(1, $role_id)) { //pemi

            $items = MultirotatorSchedule::with('Multirotator','Mall','Subscriber')->join('multirotators','multirotatorschedules.multirotator_id','=','multirotators.id')->where('multirotators.is_active', '=', 1)->get();

            return view('.manage.multirotator')->with('items', $items);

        } else { //mall-admin || subscriber
            //check malls associated with user
            $malls = MallUser::with('Mall')->get();
            $mall_id = array();
            
            foreach($malls as $mall){
               array_push($mall_id , $mall->mall_id);
            }

            $items = MultirotatorSchedule::whereIn('mall_id', $mall_id)->join('multirotators','multirotatorschedules.multirotator_id','=','multirotators.id')->where('multirotators.is_active', '=', 1)->get();
            return view('.manage.multirotator')->with('items', $items);
        }
    }

    public function Additems(Request $request){
        $user_id = Auth::User()->id;
        $role_id = $this->getRole();

        if (in_array(1, $role_id)) { //pemi
            
            //get all malls
            //$malls = Mall::pluck('name','id')->reverse()->put('', '-----')->reverse();
            $malls = Mall::orderBy('name')->get();
            $subscribers = Subscriber::pluck('name', 'id')->reverse()->put('', '-----')->reverse();
            $shops = Shop::pluck('title', 'id')->reverse()->put('', '-----')->reverse();

        } else { //subscriber || mall-admin

            //get all malls associated with the user
            //$malls = MallUser::where('user_id','=',$user_id)->join('malls', 'malls_users.mall_id','=', 'malls.id')->pluck('malls.name','mall_id')->reverse()->put('', '-----')->reverse();          
            $malls = MallUser::where('user_id','=',$user_id)->join('malls', 'malls_users.mall_id','=', 'malls.id')->select('mall_id AS id','name','account_id')->get();

            //a user maybe associate with more than 1 subscriber
            $subscribers = SubscriberUser::where('user_id', '=', $user_id)->where('subscribers_users.is_active','=',1)->join('subscribers','subscribers_users.subscriber_id','=','subscribers.id')->pluck('subscribers.name', 'subscriber_id')->reverse()->put('', '-----')->reverse();

            $subscriber_id = SubscriberUser::where('user_id', '=', $user_id)->where('subscribers_users.is_active','=', 1)->join('subscribers','subscribers_users.subscriber_id','=','subscribers.id')->pluck('subscriber_id');

            $shops = ShopSubscriber::with('Shop')->whereIn('subscriber_id', $subscriber_id)->join('shops','shops_subscribers.shop_id','=','shops.id')->pluck('shops.title','shop_id')->reverse()->put('', '-----')->reverse();

            // get subscription
            $subscriptions = SubscriberSubscription::with('Subscription')->whereIn('subscription_id', [2,3])->whereIn('subscriber_id', $subscriber_id)->where('is_active','=', 1)->pluck('name','id')->reverse()->put('', '-----')->reverse();
            
            /*
            // get consumed credits
            $consumed = DB::table('multirotatorschedules')
                                    ->select(DB::raw('multirotatorschedules.mall_id, subscribers_subscriptions.advertising_credit -  datediff(max(date(multirotatorschedules.end_date)) + 1 , min(date(multirotatorschedules.start_date))) as remaining_credit'))
                                    ->leftJoin('subscribers_subscriptions', 'multirotatorschedules.subscriber_id', '=', 'subscribers_subscriptions.subscriber_id')
                                    ->whereIn('multirotatorschedules.subscriber_id', $subscriber_id)
                                    ->groupBy('subscribers_subscriptions.advertising_credit', 'multirotatorschedules.mall_id')
                                    ->get();
            */

            //return $subscriptions;
          
        }

        return view('.manage.add-multirotator', ['malls' => $malls, 'subscribers' => $subscribers, 'shops' => $shops, 'subscriptions' => $subscriptions]);
    }

    public function UpdateItems(Request $request){
        $user_id = Auth::User()->id;
        $role_id = $this->getRole();

        $malls = array();
        $input = $request->input('selected');
        $count = count($input);
    
        switch($request->submit) {

            case 'Delete': 
                Multirotator::whereIn('id', $input)->update(['is_active' => 0, 'deleted_by' => $user_id]);
                return redirect()->action('MultirotatorController@ShowItems');
            break;

            case 'Update': 
               
                if($count > 1){
                    
                    return Redirect::back()->withErrors(['Please select only 1 item to update']);

                } else {

                    $items = MultirotatorSchedule::with('Mall','Multirotator')->whereIn('id', $input)->get();
                    if (in_array(1, $role_id)) { //admin
                        //get all malls
                        $malls = Mall::pluck('name','id')->reverse()->put('', '-----')->reverse();
                        $subscribers = Subscriber::pluck('subscribers.name', 'id')->reverse()->put('', '-----')->reverse();

                        $shops = MallShop::with('Shop')->orderBy('shops.title')->join('shops','malls_shops.shop_id','=','shops.id')->pluck('shops.title','shops.id')->reverse()->put('', '-----')->reverse();

                         return view('.manage.update-multirotator', array('items'=> $items, 'malls' => $malls, 'subscribers' => $subscribers, 'shops' => $shops));      

                    }else { //non-admin
                        //get all malls associated with the user
                        $malls = MallUser::where('user_id','=',$user_id)->join('malls', 'malls_users.mall_id','=', 'malls.id')->pluck('malls.name','mall_id')->reverse()->put('', '-----')->reverse();          

                        //a user maybe associate with more than 1 subscriber
                        $subscribers = SubscriberUser::where('user_id', '=', $user_id)->join('subscribers','subscribers_users.subscriber_id','=','subscribers.id')->pluck('subscribers.name', 'subscriber_id')->reverse()->put('', '-----')->reverse();

                        $subscriber_id = SubscriberUser::where('user_id', '=', $user_id)->join('subscribers','subscribers_users.subscriber_id','=','subscribers.id')->pluck('subscriber_id');

                        $shops = ShopSubscriber::with('Shop')->whereIn('subscriber_id', $subscriber_id)->join('shops','shops_subscribers.shop_id','=','shops.id')->pluck('shops.title','shop_id')->reverse()->put('', '-----')->reverse();
                            }

                        return view('.manage.update-multirotator', array('items'=> $items, 'malls' => $malls, 'subscribers' => $subscribers, 'shops' => $shops));           
                        //return $items;        
                }    

            break;
        }
    }

    public function SaveItems(Request $request){

        //$this->ValidateFormInput($request);

        $this->validate($request, [
            'subscriptions' => 'required',
            'name' => 'required',
            'duration' => 'required',
            'malls' => 'required',
            'subscribers' => 'required',
            'shops' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
            'image' => 'required'
        ]);

        $name =  $request->input('name');
        $duration =  $request->input('duration');
        $mall_id =  $request->input('malls');
        $subscriber_id = $request->input('subscribers');
        $shop_id = $request->input('shops');
        $show_from =  date('Y-m-d H:i:s', strtotime($request->input('start_date')));
        $show_to =  date('Y-m-d H:i:s', strtotime($request->input('end_date')));
        $image =  $request->input('image');
        $subscription_id = $request->input('subscriptions');
        $count = count($mall_id)-1;

        $path = './public/images/multirotator';
        $md5Name = md5_file($request->file('image')->getRealPath());
        $guessExtension = $request->file('image')->guessExtension();
        $file = $request->file('image')->storeAs($path, $md5Name.'.'.$guessExtension);
        $created_at = Carbon::now();
        $updated_at = Carbon::now();


        // check if still have advertising credit
        $advertising_credit = SubscriberSubscription::whereIn('subscription_id', [2,3])->where('is_active','=', 1)->where('subscriber_id', '=', $subscriber_id)->pluck('advertising_credit');

        // get remaining credits
        /*$remaining_credit = DB::table('subscription_logs')
                                    ->select(DB::raw('IFNULL(' . $advertising_credit[0] . ' - sum(consumed_advertising_credit), 0) remaining'))
                                    ->where('subscriber_subscription_id', '=', $subscription_id)
                                    ->get();
        */

        $sum_consumed = DB::table('subscription_logs')
                                ->select(DB::raw('IFNULL(sum(consumed_advertising_credit), 0) AS rem'))
                                ->where('subscriber_subscription_id', '=', $subscription_id)
                                ->pluck('rem');

        
        if($sum_consumed[0] == '0'){ 
            $sum = 0; 
        }else {
            $sum = $sum_consumed[0];
        }
        $remaining_credit = $advertising_credit[0] - $sum;
                                    
        $diff=date_diff(date_create($show_to) , date_create($show_from));
        $consumed_advertising_credit = $diff->format("%a") + 1;                 

        if($remaining_credit > 0){

            $multirotator_id = Multirotator::insertGetId([
                'name' => $name,
                'image' => $md5Name.'.'.$guessExtension,
                'duration' => $duration,
                'created_at' => $created_at,
                'updated_at' => $updated_at
            ]);

            $i=0; //for loop counter
            for($i=0; $i <= $count; $i++){
                $multirotatorschedules[$i] = [
                    'mall_id' => $mall_id[$i],
                    'multirotator_id' => $multirotator_id,
                    'shop_id' => $shop_id, 
                    'start_date' => $show_from,
                    'end_date' => $show_to,
                    'subscriber_id' => $subscriber_id
                ];

                try{
                       MultirotatorSchedule::insert($multirotatorschedules[$i]);
                    } catch (\Illuminate\Database\QueryException $e) {
                       return view('exceptions');
                    } catch (PDOException $e) {
                        return view('exceptions');
                    }
            }

            $subscriptionlogs = [
                'subscriber_subscription_id' => $subscription_id,
                'consumed_advertising_credit' => $consumed_advertising_credit,
                'created_at' => $created_at,
                'updated_at' => $updated_at
            ];

            SubscriptionLog::insert($subscriptionlogs);

            return redirect()->action('MultirotatorController@ShowItems');

        } else {

            return view('no-advertising-credit');
            
        }

    }

    public function SaveUpdatedItems(Request $request){
        
        $this->ValidateFormInputNoImage($request);

        $id = $request->input('id');
        $multirotator_id = $request->input('multirotator_id');
        $mall_id =  $request->input('malls');
        $subscriber_id = $request->input('subscribers');
        $shop_id = $request->input('shops');
        $name =  $request->input('name');
        $duration = $request->input('duration');
        $start_date = $request->input('start_date');
        $end_date = $request->input('end_date');
        
        /*$count = count($id)-1;
        
        $i=0; //for loop counter
        for($i=0; $i <= $count; $i++){
            $multirotators[] = [
                'name' => $name[$i],
                'duration' => $duration[$i],
                ];

            Multirotator::where('id', '=' ,$multirotator_id[$i])->update($multirotators[$i]);

            $multirotatorschedules[] = [
                'mall_id' => $mall_id[$i],
                'multirotator_id' => $multirotator_id[$i],
                'shop_id' => $shop_id[$i], 
                'start_date' => $start_date[$i],
                'end_date' => $end_date[$i],
                'subscriber_id' => $subscriber_id[$i]   
            ];

            MultirotatorSchedule::where('id', '=' ,$ids[$i])->update($multirotatorschedules[$i]);
        }*/

        $multirotators = [
            'name' => $name,
            'duration' => $duration,
        ];

        Multirotator::where('id', '=' ,$multirotator_id)->update($multirotators);

        $multirotatorschedules = [
            'mall_id' => $mall_id,
            'multirotator_id' => $multirotator_id,
            'shop_id' => $shop_id, 
            'start_date' => $start_date,
            'end_date' => $end_date,
            'subscriber_id' => $subscriber_id,
        ];

        //MultirotatorSchedule::where('id', '=' ,$id)->update($multirotatorschedules);
      
        return redirect()->action('MultirotatorController@ShowItems');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Product;
use App\RoleUser;
use App\Shop;
use Carbon\Carbon;
use App\Mall;
use App\MallProduct;
use App\Malluser;
use App\SubscriberUser;
use App\ShopSubscriber;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Database\QueryException;

class ProductController extends Controller
{

	public function __construct()
    {
        $this->middleware('auth');
    }

    public function getRole(){

        //get id of logged in user
        $user_id = Auth::User()->id;
        $role_id = array();
        //get role of user. 1 => admin, 2 => subscriber
       
        $roles = RoleUser::where('user_id', '=', $user_id)->get();
        foreach ($roles as $role) {
            array_push($role_id, $role->role_id);
        }

        return $role_id;
    }

    public function ValidateFormInput($request){
        return $this->validate($request, [
            'title' => 'required',
            'shop' => 'required',
            'description' => 'required',
            'image_thumb' => 'required',
            'image_large' => 'required',
            'show_from' => 'required',
            'show_to' => 'required',
            'is_active' => 'required',
            'is_promo' => 'required',
        ]);
    }

    public function ValidateFormInputNoImage($request){
        return $this->validate($request, [
            'title' => 'required',
            'shop' => 'required',
            'description' => 'required',
            'show_from' => 'required',
            'show_to' => 'required',
            'is_active' => 'required',
            'is_promo' => 'required',
        ]);
    }

    public function ShowItems(){

    	$user_id = Auth::User()->id;
        $role_id = $this->getRole();

        if (in_array(1, $role_id)) { //pemi

        	$items = Product::with('Shop')->where('is_active', '=', 1)->get();
    	
    	   	return view('.manage.product', ['items' => $items]);
    	   	//return $items;

        } else if (in_array(2, $role_id)){ //subscriber
            
            //get malls associated with user -> $mall_array
            //get subscribers associated with user -> $subscriber_array
            //get shops associated with subscriber
            //get products associated with that shop

            //check malls associated with user
            $malls = MallUser::with('Mall')->where('user_id', '=', $user_id)->get();
            
            //make an array of malls for easy query
            $mall_array = array();
            
            foreach($malls as $mall){
               array_push($mall_array , $mall->mall_id);
            }
            
            // get subscribers associated with that user
            $subscribers = SubscriberUser::with('Subscriber')->where('user_id', '=', $user_id)->get();

            $subscriber_array  = array();

            foreach($subscribers as $subscriber){
                array_push($subscriber_array, $subscriber->subscriber_id);
            }

            // get shops associated with that subscriber
            $shops = ShopSubscriber::with('Shop')->whereIn('subscriber_id', $subscriber_array)->get();

            $shop_array = array();

            foreach($shops as $shop){
                array_push($shop_array, $shop->shop_id);
            }

            // get products associated with shops
            $items = Product::with('Shop')->whereIn('shop_id',$shop_array)->where('is_active', '=', 1)->get();
            
            return view('.manage.product')->with('items', $items);
            //return $items;

        } else { // mall-admin

            return view('no-permissions');
        }
    	
    }

    public function AddItems(){
    	//TODO: alter this based on user role
    	//TODO: get only shops that have subscriptions
    	$shops = Shop::pluck('title','id')->reverse()->put('', '-----')->reverse();
    	return view('manage.add-product', ['shops' => $shops]); 
    }

    public function SaveItems(Request $request){

        $this->ValidateFormInput($request);

    	$title = $request->input('title');
    	$shop_id = $request->input('shop');
    	$description = $request->input('description');
    	
    	$path = './public/images/product';
        $md5Name_thumb = md5_file($request->file('image_thumb')->getRealPath());
        $guessExtension_thumb = $request->file('image_thumb')->guessExtension();
        $file_thumb = $request->file('image_thumb')->storeAs($path, $md5Name_thumb.'.'.$guessExtension_thumb);

        $md5Name_enlarged = md5_file($request->file('image_large')->getRealPath());
        $guessExtension_enlarged = $request->file('image_large')->guessExtension();
        $file_thumb = $request->file('image_large')->storeAs($path, $md5Name_enlarged.'.'.$guessExtension_enlarged);

    	$show_from =  date('Y-m-d H:i:s', strtotime($request->input('show_from')));
        $show_to =  date('Y-m-d H:i:s', strtotime($request->input('show_to')));
    	
    	$is_active = $request->input('is_active');
    	$is_promo = $request->input('is_promo');
    	$created_at = Carbon::now();
        $updated_at = Carbon::now();

        $dateSet = [
        	'title' => $title,
        	'img_thumb' => $md5Name_thumb.'.'.$guessExtension_thumb,
        	'img_enlarged' => $md5Name_enlarged.'.'.$guessExtension_enlarged,
        	'show_from' => $show_from,
        	'show_to' => $show_to,
        	'created_at' => $created_at,
        	'updated_at' => $updated_at,
        	'shop_id' => $shop_id,
        	'is_active' => $is_active,
        	'is_promo' => $is_promo,
        	'description' => $description
        ];

        Product::insert($dateSet);

        return redirect()->action('ProductController@ShowItems');
    }

    public function UpdateItems(Request $request){

        $user_id = Auth::User()->id;
        $role_id = $this->getRole();

        $malls = array();
        $input = $request->input('selected');
        $count = count($input);

        if($count < 1){
            return Redirect::back()->withErrors(['Please select item(s) to modify']);
        } else {
    
            switch($request->submit) {

                case 'Delete': 
                    Product::whereIn('id', $input)->update(['is_active' => 0, 'deleted_by' => $user_id]);
                    return redirect()->action('ProductController@ShowItems');

                case 'Update': 
                   
                    $items = Product::with('Shop')->whereIn('id', $input)->get();

                    if (in_array(1, $role_id)) { //admin
                        //get all shops
                        $shops = Shop::pluck('title','id')->reverse()->put('', '-----')->reverse();
                        return view('manage.update-product', ['items' => $items, 'shops' => $shops]);

                    }else { //non-admin
                        //get all shops associated with the user

                        
                    }

                    return view('.manage.update-event', array('items'=> $items, 'malls' => $malls));

                case 'Associate to Mall':
                    if($count > 1){

                        return Redirect::back()->withErrors(['Please select only 1 subscriber to update']);

                    } else{

                         if (in_array(1, $role_id)) { //pemi
                            // Multiple updates not allowed here
                           
        		              $items = Product::with('Shop')->whereIn('id', $input)->get();
                              $mallproduct = MallProduct::with('Mall')->whereIn('product_id', $input)->where('is_active','=',1)->get();
        		                //$malls = Mall::pluck('name','id')->reverse()->put('', '-----')->reverse();
        		              $malls = Mall::all();

                   	          return view('manage.add-product-to-mall', ['items' => $items, 'malls' => $malls, 'mallproduct' => $mallproduct]) ;
                              //return $mallproduct;
                            
                        } else { // subscriber
                            // get only malls associated with the user
                            $items = Product::with('Shop')->whereIn('id', $input)->get();
                            //$malls = Mall::pluck('name','id')->reverse()->put('', '-----')->reverse();
                            $malls = MallUser::where('user_id','=',$user_id)->join('malls', 'malls_users.mall_id','=', 'malls.id');
                            return view('manage.add-product-to-mall', ['items' => $items, 'malls' => $malls]) ;
                        }
                    }
    		            
                break;
            }
        }

    }

    public function SaveUpdatedItems(Request $request){

        $this->ValidateFormInputNoImage($request);

    	$id = $request->input('id');
    	$title = $request->input('title');
    	$show_from = $request->input('show_from');
        $show_to = $request->input('show_to');
        $count = count($id)-1;
    	$shop_id = $request->input('shop');
    	$description = $request->input('description');    	
    	$is_active = $request->input('is_active');
    	$is_promo = $request->input('is_promo');
        $updated_at = Carbon::now();
        
        /*$count = count($id)-1;

        $i=0; //for loop counter
        for($i=0; $i <= $count; $i++){
            $dataSet[$i] = [
	        	'title' => $title[$i],
	        	'show_from' => date('Y-m-d H:i:s', strtotime($show_from[$i])),
                'show_to' => date('Y-m-d H:i:s', strtotime($show_to[$i])),
	        	'updated_at' => $updated_at,
	        	'shop_id' => $shop_id[$i],
	        	'is_active' => $is_active[$i],
	        	'is_promo' => $is_promo[$i],
	        	'description' => $description[$i]
            ];

            Product::where('id', '=' ,$id[$i])->update($dataSet[$i]);
        }*/

        $dataSet= [
            'title' => $title,
            'show_from' => date('Y-m-d H:i:s', strtotime($show_from)),
            'show_to' => date('Y-m-d H:i:s', strtotime($show_to)),
            'updated_at' => $updated_at,
            'shop_id' => $shop_id,
            'is_active' => $is_active,
            'is_promo' => $is_promo,
            'description' => $description
        ];

        Product::where('id', '=' ,$id)->update($dataSet);

        return redirect()->action('ProductController@ShowItems');
    }

    public function AddProductToMall(Request $request){

       $this->validate($request, [
            'malls' => 'required',
        ]);

    	$product_id = $request->input('product_id');
        $mall_id = $request->input('malls');
        $count = count($mall_id) - 1;
        $created_at = Carbon::now();
        $updated_at = Carbon::now();

        $i=0; //for loop counter
        for($i=0; $i <= $count; $i++){
            $dataSet[$i] = [
                'product_id' => $product_id,
                'mall_id' => $mall_id[$i],
                'is_active' => 1,
                'created_at' => $created_at,
                'updated_at' => $updated_at
            ];  
            
            try{
                
                MallProduct::insert($dataSet[$i]);
                
            } catch (\Illuminate\Database\QueryException $e) {
                //TODO: make this work! will show error message
                //return Redirect::back()->withErrors(['Selected mall is already associated with the product!']);

            } catch (PDOException $e) {
                
            }  
        }
      
        return redirect()->action('ProductController@ShowItems'); 

        // return $product_id;
    }

    public function RemoveMall($id){
        $product = Product::where('id','=', $id)->get();
        $items = MallProduct::with('Mall')->where('product_id','=', $id)->where('is_active','=','1')->get();
        return view('manage.remove-mall-from-product', ['items' => $items, 'product' => $product]);
    }

    public function SaveRemovedMall(Request $request){

        $this->validate($request, [
            'selected' => 'required'
        ]);

        $deleted_by = Auth::User()->id;
        $product_id = $request->input('product_id');
        $mall_id = $request->input('selected');

        //MallProduct::where('product_id', $product_id)->whereIn('mall_id', $mall_id)->update(['is_active' => 0, 'deleted_by' => $deleted_by]);

        // Totatlly remove the record instead of soft delete
        MallProduct::where('product_id', $product_id)->whereIn('mall_id', $mall_id)->delete();
        return redirect()->action('ProductController@ShowItems');
    }
}

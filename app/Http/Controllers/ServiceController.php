<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Service;

class ServiceController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }

    public function showItems(){
    	//$items = Event::get();
    	return view('.manage.service');
    	//return $items;
    }
}

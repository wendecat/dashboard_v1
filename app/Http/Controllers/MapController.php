<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Map;

class MapController extends Controller
{
	
	public function __construct()
    {
        $this->middleware('auth');
    }

    public function showItems(){
    	return view('.manage.map');
    }
}



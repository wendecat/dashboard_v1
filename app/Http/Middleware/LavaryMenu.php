<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\RoleUser;
use App\Subscriber;
use App\Subscription;
use App\SubscriberSubscription;
use App\SubscriberUser;

class LavaryMenu
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    public function handle($request, Closure $next)
    {

      if(!Auth::check()){
        //$user_id = Auth::User()->id;
      }

      \Menu::make('MyNavBar', function($menu){
		  
		    $menu->add('Home', array('route'  => 'home'));
	
      	// set a variable 
    		$manage = $menu->add(trans('Manage'), ['class' => 'dropdown']);
    		$manage->link->attr(['class' => 'dropdown-toggle', 'data-toggle' => 'dropdown']);

  	    // add children items to $manage
  	    $manage->add('Accounts', 'manage/account');
  	    $manage->add('Category', 'manage/category');
    		$manage->add('Event','manage/event');
    		$manage->add('Kiosk','manage/kiosk');
        $manage->add('Mall-Shop','manage/mallshop');
    		$manage->add('Map','manage/map');
    		$manage->add('Multirotator','manage/multirotator');
    		$manage->add('Products','manage/product');
    		$manage->add('Promo','manage/promo');
    		$manage->add('Service','manage/service');
    		$manage->add('Shop','manage/shop');
        $manage->add('Subscriber','manage/subscriber');
    		$manage->add('Subscription','manage/subscription');
    		$manage->add('Tags','manage/tag');
        $manage->add('User','manage/user');
        //$manage->add('User Approval','manage/user/approve'); 

    		$menu->add('Kiosk Status',  array('route'  => 'kioskStatus'));
    		$menu->add('Approvals',  array('route'  => 'approvals')); //approved upload of items
     		$menu->add('Reports',  array('route'  => 'reports'));
    		  
    		});
    		
    		return $next($request);
    }
	

}

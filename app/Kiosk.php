<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kiosk extends Model
{
    public function Mall(){
    	return $this->belongsTo('App\Mall');
    }
}
